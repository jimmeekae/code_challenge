@extends('app')

@section('title', 'Tasks : Projects')

@php $page="projects" @endphp



@section('header')
 @include('layouts.header')
 @endsection
@section('sidebar')
    @include('layouts.sidebar')
    @endsection

    <!-- Content Wrapper. Contains page content -->
 @section('content')

 <script src="{{ asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });


    function submitform() {
        document.myform.submit();
    }


    $('.notify').click(function () {
        var id = $(this).attr('id');
        var token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: "post",
            url: "/clear-notification",
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                'id': id,
                _token: token
            },
            success: function (s) {


                if (s.status == 'message') {


                    $('#mess').html('<input name="msg" id="msg" type="hidden" value="1">')
                    submitform();
                }
                else {
                    window.location.replace('mytask');

                }
            }
        });
    });
</script>
       <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                                <h1>
                    Admin
                    <small>Control panel</small>
                </h1>
                
                <div class="table_box">
                    <div class="box-header">
                        <div style="margin-bottom:2em" class="col-md-12">
                        <h3 class="box-title">Project Management</h3>
                        </div>
                                                    <div class="btn-group btn-group-sm pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                                        data-target="#create-project"><i class="fa fa-plus"></i> Add Project
                                </button>
                            </div>
                        
                        <div class="example-modal modal fade" id="create-project" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-success">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close close_btn" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Add New Project</h4>
                                        </div>
                                        <div style="padding-left:20px;padding-right:20px" class="modal-body">
                                            <form data-toggle="validator" action=""
                                                  method="POST">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="name">Project Name </label>

                                                            <input autocomplete="off" type="text" id="name" name="name"
                                                                   class="form-control" placeholder="project name">

                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="client_id">Team Leader </label>

                                                            <select class="form-control" id="leader" name="leader">
                                                            @php $id=Auth::user()->id; $users=get_other_users($id);@endphp
                                                                <option value="" selected disabled hidden>select Leader
                                                                    id
                                                                </option>
                                                                @foreach($users as $user)
                                                <option value="{{$user->name}}">2012-1-60-040
                                                                        &nbsp;&nbsp; {{$user->name}}</option>@endforeach

                                                                                                                                    <
                                                                                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name">Status </label>
                                                            <select class="form-control" id="status" name="status">
                                                                <option value="" disabled hidden selected>status
                                                                </option>
                                                                @php $status=get_status();@endphp
                                                                @foreach($status as $stat)
                                                                <option value="{{$stat->name}}">{{$stat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">Details</label>
                                                            <textarea rows="10" style="resize:none" name="details"
                                                                      id="details"
                                                                      class="form-control"
                                                                      placeholder="Tell me details about project "></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div style="display:none;" class="form-group"
                                                                 id="name_error"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-add">Add
                                                Project
                                            </button>
                                            <button type="submit" class="btn btn-outline close_btn"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- /.example-modal -->

                        <!-- /edit modal-->
                        <div class="example-modal modal fade" id="edit-project" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-primary">
                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Edit Project</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form action=""
                                                  method="POST">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="name">Project Name </label>

                                                            <input autocomplete="off" type="text" id="name" name="name"
                                                                   class="form-control">

                                                        </div>

                                                        <input autocomplete="off" type="text" id="id" name="id"
                                                               class="form-control hidden">

                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="leader">Team Leader </label>

                                                            <select class="form-control" id="leader" name="leader">
                                                                                                                                    <option value="Awlad de leo,2012-1-60-040">Awlad de leo
                                                                        &nbsp;&nbsp;
                                                                        2012-1-60-040</option>
                                                                                                                                    <option value="Hayk,2012-1-60-000">Hayk
                                                                        &nbsp;&nbsp;
                                                                        2012-1-60-000</option>
                                                                                                                                    <option value="Ahui,aasu">Ahui
                                                                        &nbsp;&nbsp;
                                                                        aasu</option>
                                                                                                                                    <option value="Test,testclient">Test
                                                                        &nbsp;&nbsp;
                                                                        testclient</option>
                                                                                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="name">Status </label>
                                                            <select class="form-control" id="status" name="status">
                                                                <option value="Closed">Close</option>
                                                                <option value="Open">Open</option>
                                                                <option value="Completed">Completed</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">Details</label>
                                                            <textarea rows="10" style="resize:none" name="details"
                                                                      id="details"
                                                                      class="form-control"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div style="display:none;" class="form-group"
                                                                 id="edit_error"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline pull-left crud-submit-edit">
                                            Update
                                        </button>
                                        <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                        </button>
                                    </div>

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <!-- end edit modal-->

                        <!-- /delete modal-->
                        <div class="example-modal modal fade" id="delete-project" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-danger">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Delete Category</h4>
                                        </div>
                                        <div class="modal-body">

                                            <p style="text-align: center;">Are you sure you want to delete the category
                                                ?</p>
                                            <input type="text" id="id" name="id"
                                                   class="form-control hidden">
                                            <input type="text" id="_token" name="_token"
                                                   class="form-control hidden" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-delete">
                                                Delete
                                            </button>
                                            <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end delete modal-->

                        <!-- /details modal-->
                        <div class="example-modal modal fade" id="details-project" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-primary">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Project Details</h4>
                                        </div>
                                        <div class="modal-body">

                                            <h5 style="padding-right:3%;padding-left: 3%;font-size: 15px;"></h5>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end details modal-->


                    </div>
                    <!-- /.box-header -->
                    <div id="table_category" class="box-body">
                        <table id="example2" class="table task_table table-bordered table-hover project_table">
                            <thead>
                            <tr>

                                <th>Project Name</th>
                                <th>Team Leader</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!--content-->

                            </tbody>

                        </table>
                        <ul id="pagination" class="pagination-sm"></ul>
                    </div>
                    <!-- /.box-body -->
                </div>

            </section>

        </div>
        <!-- /.content-wrapper -->

       

    @section('footer')

    @include('layouts.footer')

    @endsection

    @endsection

    <!-- REQUIRED JS SCRIPTS -->

   @section('extra-js')


<!-- jQuery 2.2.3 -->
<script src="{{ asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="{{ asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('theme/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('theme/dist/js/demo.js') }}"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('theme/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

<script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>


    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <script>
        var user_role = ['{{Auth::user()->role}}'];

    </script>

    <script src="{{ asset('js/project.js') }}"></script>


    @endsection

