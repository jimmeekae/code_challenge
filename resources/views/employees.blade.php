@extends('app')

@section('title', 'Tasks : Employees')

@php $page="employees" @endphp



@section('header')
 @include('layouts.header')
 @endsection
@section('sidebar')
    @include('layouts.sidebar')
    @endsection

    <!-- Content Wrapper. Contains page content -->
 @section('content')

 <script src="{{ asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });


    function submitform() {
        document.myform.submit();
    }


    $('.notify').click(function () {
        var id = $(this).attr('id');
        var token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: "post",
            url: "/clear-notification",
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                'id': id,
                _token: token
            },
            success: function (s) {


                if (s.status == 'message') {


                    $('#mess').html('<input name="msg" id="msg" type="hidden" value="1">')
                    submitform();
                }
                else {
                    window.location.replace('mytask');

                }
            }
        });
    });
</script>
   
         <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                                    <h1 >
                        Admin
                        <small>Control panel</small>
                    </h1>
                
                <div class="table_box">
                    <div class="box-header">
                        <div style="margin-bottom:2em" class="col-md-12">
                        <h3 class="box-title">Employee Management</h3>
                        </div>
                                                    <div class="btn-group btn-group-sm pull-right">
                                <button type="button" class="btn disabled btn-success btn-sm" data-toggle="modal"
                                        data-target="#add-member"><i class="fa fa-plus"></i> Add Member
                                </button>
                            </div>
                                        <!-- Add member Modal -->
                        <div class="example-modal modal fade" id="add-member" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-success">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close close_btn" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Add New Member</h4>
                                        </div>
                                        <div style="padding-left:20px;padding-right:20px" class="modal-body">
                                            <form data-toggle="validator"
                                                  action=http://task.exodias.com/register method="post">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="fname">First Name *</label>
                                                            <input type="text" id="fname" name="fname"
                                                                   class="form-control" placeholder="first name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">E-mail *</label>
                                                            <input type="email" id="email" name="email"
                                                                   class="form-control" placeholder="e-mail">
                                                        </div>


                                                        <div class="form-group">
                                                            <label for="position">Position </label>
                                                            <input type="text" id="position" name="position"
                                                                   class="form-control" placeholder="position">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password">Password *</label>
                                                            <input type="password" id="password" name="password"
                                                                   class="form-control" placeholder="password">
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="_Token" value="7YjwW12CTVmymSP813hCM6PJQvBx5CkDLWlXJdXz">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lname">Last Name </label>
                                                            <input type="text" id="lname" name="lname"
                                                                   class="form-control" placeholder="last name">

                                                        </div>
                                                        <div class="form-group">
                                                            <label for="user_id">User Id *</label>
                                                            <input type="text" id="user_id" name="user_id"
                                                                   class="form-control" placeholder="user id">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="sex">Sex *</label>
                                                                    <select class="form-control" id="sex" name="sex">
                                                                        <option value="" selected disabled hidden>select
                                                                            sex
                                                                        </option>
                                                                        <option value="male">Male</option>
                                                                        <option value="female">Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="role">Role *</label>
                                                                    <select class="form-control" id="role" name="role">
                                                                        <option value="" selected="selected" disabled
                                                                                hidden>select role
                                                                        </option>
                                                                        <option value="client">Client</option>
                                                                        <option value="admin">Admin</option>
                                                                    </select>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="rpassword">Confirm-Password *</label>
                                                            <input type="password" id="rpassword" name="rpassword"
                                                                   class="form-control" placeholder="confirm password">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div style="display:none;" class="form-group"
                                                                 id="name_error"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-add">Add
                                                Member
                                            </button>
                                            <button type="submit" class="btn btn-outline close_btn"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- /.example-modal -->

                        <!-- /edit modal-->
                        <div class="example-modal modal fade" id="edit-client" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-primary">
                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Edit Information</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form data-toggle="validator"
                                                  action=http://task.exodias.com/update method="post">
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="email">E-mail *</label>
                                                            <input type="email" id="email" name="email"
                                                                   class="form-control" placeholder="e-mail">
                                                        </div>
                                                        <input name="client" id="client" type="hidden" value="">


                                                        <div class="form-group">
                                                            <label for="position">Position </label>
                                                            <input type="text" id="position" name="position"
                                                                   class="form-control" placeholder="position">
                                                        </div>

                                                    </div>
                                                    <input type="hidden" name="_token" value="7YjwW12CTVmymSP813hCM6PJQvBx5CkDLWlXJdXz">


                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="user_id">User Id *</label>
                                                            <input type="text" id="user_id" name="user_id"
                                                                   class="form-control" placeholder="user id">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="role">Role *</label>
                                                            <select class="form-control" id="role" name="role">
                                                                <option value="" selected="selected" disabled hidden>
                                                                    select role
                                                                </option>
                                                                <option value="client">Client</option>
                                                                <option value="admin">Admin</option>
                                                            </select>
                                                        </div>

                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline pull-left crud-submit-edit">
                                            Update
                                        </button>
                                        <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                        </button>
                                    </div>

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <!-- end edit modal-->

                        <!-- /delete modal-->
                        <div class="example-modal modal fade" id="delete-client" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-danger">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Delete Category</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" id="id" name="id"
                                                   class="form-control hidden">
                                            <input type="text" id="_token" name="_token"
                                                   class="form-control hidden" value="7YjwW12CTVmymSP813hCM6PJQvBx5CkDLWlXJdXz">

                                            <p style="text-align: center;">Are you sure you want to delete the Member
                                                ?</p>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-delete">
                                                Delete
                                            </button>
                                            <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end delete modal-->


                    </div>
                    <!-- /.box-header -->
                    <div id="table_client" class="box-body">
                        <table id="example2" class="table table-bordered task_table table-hover ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>User Id</th>
                                <th>Position</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>


                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                        <ul id="pagination" class="pagination-sm"></ul>
                    </div>
                    <!-- /.box-body -->
                </div>

            </section>

        </div>
        <!-- /.content-wrapper -->

       

    @section('footer')

    @include('layouts.footer')

    @endsection

    @endsection

    <!-- REQUIRED JS SCRIPTS -->

   @section('extra-js')


<!-- jQuery 2.2.3 -->
<script src="{{ asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="{{ asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('theme/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('theme/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('theme/dist/js/demo.js') }}"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('theme/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

<script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>


    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <script>
        var user_role = ['{{Auth::user()->role}}'];

    </script>

    <script src="{{ asset('js/client.js') }}"></script>




    @endsection

