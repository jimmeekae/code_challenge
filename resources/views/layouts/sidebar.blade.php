<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">

                <img src="{{ asset('theme/dist/img/1528135843.jpg') }}" class="img-circle" alt="User Image">

            </div>
            <div class="pull-left info">
                <p>{{  Auth::user()->name }} </p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
        @if( Auth::user()->role=="admin")
            <li class="header">Navigation</li>
            <!-- Optionally, you can add icons to the links -->
            <li  @if($page=="dashboard") class="active" @endif>
                <a href="/dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
            </li>
            <li @if($page=="task") class="active" @endif>
                <a href="/tasks"><i class="fa fa-folder-open-o"></i> <span>Tasks</span></a>
            </li>
            <li @if($page=="mytask") class="active" @endif>
                <a href="/mytask"><i class="fa fa-tasks"></i> <span>My Tasks</span></a>
            </li>

            <li  @if($page=="categories") class="active" @endif>
                <a href="/categories"><i class="fa fa-th-list"></i> <span>Categories</span></a>
            </li>

            <li  @if($page=="projects") class="active" @endif>
                <a href="/projects"><i class="fa fa-tasks"></i> <span>Projects</span></a>
            </li>


             <li  @if($page=="reports") class="active" @endif>
                <a href="/reports"><i class="fa fa-bar-chart-o"></i> <span>Reports</span></a>
            </li>


            <li @if($page=="employees") class="active" @endif>
                <a href="/employees"><i class="fa fa-user"></i> <span>Employees</span></a>
            </li>

            <li  @if($page=="departments") class="active" @endif>
                <a href="/departments"><i  class="fa fa-building-o"></i> <span>Departments</span></a>
            </li>


            <li class="treeview">
                <a href="#"><i class="fa fa-balance-scale"></i> <span>Priority</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">

                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 1 </span><i
                                    class="fa fa-battery-4"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 2 </span><i
                                    class="fa fa-battery-3"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 3 </span><i
                                    class="fa fa-battery-2"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 4 </span><i
                                    class="fa fa-battery-1"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 5 </span><i
                                    class="fa fa-battery-0"></i></a></li>


                </ul>
            </li>

           @endif

            @if( Auth::user()->role=="user")

            <li class="header">Navigation</li>
            <!-- Optionally, you can add icons to the links -->
            <li  @if($page=="dashboard") class="active" @endif>
                <a href="/dashboard"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
            </li>

            <li @if($page=="mytask") class="active" @endif>
                <a href="/mytask"><i class="fa fa-tasks"></i> <span>My Tasks</span></a>
            </li>

            <li  @if($page=="categories") class="active" @endif>
                <a href="/categories"><i class="fa fa-th-list"></i> <span>Categories</span></a>
            </li>

            <li  @if($page=="projects") class="active" @endif>
                <a href="/projects"><i class="fa fa-tasks"></i> <span>Projects</span></a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-balance-scale"></i> <span>Priority</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">

                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 1 </span><i
                                    class="fa fa-battery-4"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 2 </span><i
                                    class="fa fa-battery-3"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 3 </span><i
                                    class="fa fa-battery-2"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 4 </span><i
                                    class="fa fa-battery-1"></i></a></li>
                    <li><a href="javascript:void(0)"><span id="priority">Priority Level 5 </span><i
                                    class="fa fa-battery-0"></i></a></li>


                </ul>
            </li>

            @endif

        </ul>

        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>