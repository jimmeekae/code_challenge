<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Task Portal | @yield('title', '')</title>

   @section('css')
        <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="{{ asset('theme/dist/img/fevicon.jpg') }}">
    <!-- Bootstrap 3.3.6 -->


    <link rel="stylesheet" href="{{ asset('theme/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('theme/dist/css/AdminLTE.min.css') }}">
    
    <link rel="stylesheet" href="{{ asset('theme/dist/css/skins/skin-blue.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/stylesheet/style.css') }}">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="{{ asset('theme/plugins/fullcalendar/fullcalendar.min.css') }}">
  
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('theme/dist/css/AdminLTE.min.css') }}">
    <link href = "{{ asset('css/jquery-ui.css') }}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link href = "{{ asset('js/task.js') }}">
    <style type="css">
         td{
    border:1px solid #000;
}

tr td:last-child{
    width:1%;
    white-space:nowrap;
}
    </style>
    
@show
       
    
    </head>


<body class="hold-transition skin-blue sidebar-mini">
@yield('header')
@yield('sidebar')

<div class="wrapper">
  
    @yield('content')

    @yield('footer')

    @yield('extra-js')
 </div>   

</body>
</html>
