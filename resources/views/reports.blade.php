@extends('app')

@section('title', 'Tasks : Reports')

@php $page="reports" @endphp



@section('header')
 @include('layouts.header')
 @endsection
@section('sidebar')
    @include('layouts.sidebar')
    @endsection

    <!-- Content Wrapper. Contains page content -->
 @section('content')

 <script src="{{ asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });


    function submitform() {
        document.myform.submit();
    }


    $('.notify').click(function () {
        var id = $(this).attr('id');
        var token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: "post",
            url: "/clear-notification",
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                'id': id,
                _token: token
            },
            success: function (s) {


                if (s.status == 'message') {


                    $('#mess').html('<input name="msg" id="msg" type="hidden" value="1">')
                    submitform();
                }
                else {
                    window.location.replace('mytask');

                }
            }
        });
    });
</script>
   
       <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Admin
                    <small>Control panel</small>
                </h1>

                <div class="table_box">
                    <div class="box-header">
                        <div style="margin-bottom:2em" class="col-md-12">
                            <h3 class="box-title">Reports</h3>
                        </div>


                        <div class="btn-group btn-group-sm pull-right dropdown">

                            

                            <ul class="dropdown-menu nav_lu">

                                <li class="filter_btn" value="1"><a href="#"><i class="fa fa-tasks"
                                                                                aria-hidden="true"></i>Task Name</a>
                                </li>

                                <li class="filter_btn" value="2"><a href="#"><i class="fa fa-database"
                                                                                aria-hidden="true"></i>Category</a></li>

                                <li class="filter_btn" value="3"><a href="#"><i class="fa fa-calendar"
                                                                                aria-hidden="true"></i>Date</a></li>

                                <li class="filter_btn" value="4"><a href="#"><i class="fa fa-facebook"
                                                                                aria-hidden="true"></i>Status</a></li>

                                <li class="filter_btn" value="5"><a href="#"><i class="fa fa-user"
                                                                                aria-hidden="true"></i>Client</a></li>

                                <li class="filter_btn" value="6"><a href="#"><i class="fa fa-battery-full"
                                                                                aria-hidden="true"></i>Priority</a></li>
                            </ul>

                            

                        </div>


                        <div class="example-modal modal fade" id="create-item" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-success">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close close_btn" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Add New Task</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form data-toggle="validator" action="/item-task"
                                                  method="POST">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="name">Task Name </label>
                                                           <input type="text" id="name" name="name" class="form-control" placeholder="task name">

                                                            <div id="name_required"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="start_date">Start Date </label>
                                                            <input  type="date" id="start_date" name="start_date"
                                                                   class="form-control" placeholder="mm/dd/yyy">
                                                        </div>

                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="category">Category</label>
                                                            <select class="form-control" id="category" name="category"
                                                                    placeholder="select category">
                                                                <option value="" disabled hidden selected>category
                                                                </option>
                                                                @foreach ($categories as $category)
                                                             <option value="{{$category->id}}">{{ $category->name }}</option>
                                                             @endforeach
                                                        
                                                        </select>

                                                        </div>
                                                        <div class="form-group">
                                                            <label for="end_date">End Date</label>
                                                            <input type="date" id="end_date" name="end_date"
                                                                   class="form-control" value=""
                                                                   placeholder="mm/dd/yyy">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="status">Status </label>
                                                            <select class="form-control" id="status" name="status">
                                                                <option value="" disabled hidden selected>status
                                                                </option>
                                                                @foreach ($status as $stat)
                                                                <option value="{{$stat->id}}">{{$stat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="Priority">Priority </label>
                                                            <input name="pri" id="pri" class="form-control" value=""
                                                                   placeholder="1 - 5"
                                                                   onkeypress="return isNumeric(event)"
                                                                   oninput="maxLengthCheck(this)"
                                                                   type="number"
                                                                   maxlength="1"
                                                                   min="1"
                                                                   max="5"/>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="client_id">Assign To</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="client_id">
                                                                <option value="" selected disabled hidden>employee
                                                            
                                                                </option>
                                                                @foreach($users as $user)
                                <option value="{{$user->id}}">  {{$user->id}}
                                                                        &nbsp;&nbsp; {{$user->name}}</option>
                                                                        @endforeach
                                 
                                     </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="client_id">Access</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="access">
                                                                <option value="" selected disabled hidden>access
                                                            
                                                                </option>
                                                                @foreach($access_types as $access_type)
                                <option value="{{$access_type->id}}">{{$access_type->name}}</option>
                                 @endforeach
                                 
                                     </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="client_id">Percent Done</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="perc_done">
                                                                <option value="0 %" selected disabled hidden>0 %
                                                            
                                                                </option>
                                <option value="0 %">0 %</option>
                                 <option value="20 %">20 %</option>
                                <option value="50 %">50 %</option>
                                <option value="70 %">70 %</option>
                                <option value="100 %">100 %</option>
                                     </select>
                                                        </div>
                                                    </div>

                                                     <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="client_id">Tag Users</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="checkboxes" onclick="showCheckboxes()">
                                                                <option value="" selected disabled hidden>employees
                                                            
                                                                </option>
                                
                                     </select>

                                      <div id="checkboxes">
    @foreach($users as $user)
      <label for="{{$user->id}}">
  <input type="checkbox" id="{{$user->id}}" value="{{$user->id}}" name="tag_user"/> {{$user->name}} </label>
  @endforeach
    </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">Description</label>
                                                            <textarea style="resize:none" name="description"
                                                                      id="description" class="form-control"
                                                                      placeholder="Tell me details about your task "></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div style="display:none;" class="form-group"
                                                                 id="name_error"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-add">Add
                                                Task
                                            </button>
                                            <button type="submit" class="btn btn-outline close_btn"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- /.example-modal -->

                        <!-- /edit modal-->
                        <div class="example-modal modal fade" id="edit-item" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-primary">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Edit Task</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form data-toggle="validator" action=updatetask method="post">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="name">Task Name </label>
                                                           <input type="text" id="name" name="name" class="form-control" placeholder="task name">

                                                            <div id="name_required"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="start_date">Start Date </label>
                                                            <input  type="date" id="start_date" name="start_date"
                                                                   class="form-control">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="category">Category</label>
                                                            <select class="form-control" id="category" name="category"
                                                                    placeholder="select category">
                                                                
                                                                @foreach ($categories as $category)
                                                             <option value="{{$category->id}}">{{ $category->name }}</option>
                                                             @endforeach
                                                        
                                                        </select>

                                                        </div>
                                                         <div class="form-group">
                                                            <label for="end_date">End Date</label>
                                                            <input type="date" id="end_date" name="end_date"
                                                                   class="form-control" value=""
                                                                   placeholder="mm/dd/yyy">
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                         <div class="form-group">
                                                            <label for="status">Status </label>
                                                            <select class="form-control" id="status" name="status">
                                                                <option value="" disabled hidden selected>status
                                                                </option>
                                                                @foreach ($status as $stat)
                                                                <option value="{{$stat->id}}">{{$stat->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="Priority">Priority </label>
                                                            <input name="pri" id="pri" class="form-control" value=""
                                                                   placeholder="1 - 5"
                                                                   onkeypress="return isNumeric(event)"
                                                                   oninput="maxLengthCheck(this)"
                                                                   type="number"
                                                                   maxlength="1"
                                                                   min="1"
                                                                   max="5"/>

                                                        </div>
                                                    </div>

                                                    <input name="task" id="task" type="hidden" value="">
                                                    <input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="client_id">Assign To</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="client_id">
                                                                <option value="" selected disabled hidden>employee
                                                            
                                                                </option>
                                                                @foreach($users as $user)
                                <option value="{{$user->id}}">  {{$user->id}}
                                                                        &nbsp;&nbsp; {{$user->name}}</option>
                                                                        @endforeach
                                 
                                     </select>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="client_id">Access</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="access">
                                                                <option value="" selected disabled hidden>access
                                                            
                                                                </option>
                                                                @foreach($access_types as $access_type)
                                <option value="{{$access_type->id}}">{{$access_type->name}}</option>
                                 @endforeach
                                 
                                     </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="client_id">Percent Done</label>

                                                            <select class="form-control" id="client_id"
                                                                    name="perc_done">
                                                                <option value="0 %" selected disabled hidden>0 %
                                                            
                                                                </option>
                                <option value="0 %">0 %</option>
                                 <option value="20 %">20 %</option>
                                <option value="50 %">50 %</option>
                                <option value="70 %">70 %</option>
                                <option value="100 %">100 %</option>
                                     </select>
                                                        </div>
                                                    </div>



                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">Description</label>
                                                            <textarea name="description" id="description"
                                                                      class="form-control"
                                                                      placeholder="Tell me details about your task "></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div style="display:none;" class="form-group"
                                                                 id="edit_error"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-edit">
                                                Update
                                            </button>
                                            <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end edit modal-->

                        <!-- /delete modal-->
                        <div class="example-modal modal fade" id="delete-item" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-danger">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Delete Task</h4>
                                        </div>
                                        <div class="modal-body">

                                            <input type="text" id="id" name="id"
                                                   class="form-control hidden">
                                            <input type="text" id="_token" name="_token"
                                                   class="form-control hidden" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">

                                            <p style="text-align: center;">Are you sure you want to delete the task
                                                ?</p>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-delete">
                                                Delete
                                            </button>
                                            <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end delete modal-->

                        <!-- /details modal-->
                        <div class="example-modal modal fade" id="details-item" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-primary">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Description</h4>
                                        </div>
                                        <div class="modal-body">

                                            <h5 style="padding-right:3%;padding-left: 3%;font-size: 15px;"></h5>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end details modal-->


                    </div>


                    <!-- /.box-header -->
                    <div id="table_task" class="box-body search_part">
                        <div class="search_filter"></div>

                        <table style="width: 100%;white-space:nowrap;" id="example2" class="table table-bordered table-hover task_table ">
                            <thead>
                            <tr>
                                <th>Task Name</th>
                                <th>Category</th>
                                <th>Due Date</th>
                                <th>Access</th>
                                <th>Status</th>
                                <th>Assigned By</th>
                                <th>Assigned To</th>
                                <th>Perc Done</th>
                                <th>Priority</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                            
                            <tfoot></tfoot>
                            


                        </table>

                        <ul id="pagination" class="pagination-sm pagination"></ul>

                    </div>
                    <!-- /.box-body -->
                </div>

            </section>

        </div>
        <!-- /.content-wrapper -->
       

    @section('footer')

    @include('layouts.footer')

    @endsection

    @endsection

    <!-- REQUIRED JS SCRIPTS -->

   @section('extra-js')

       <!-- jQuery 2.2.3 -->
    <script src="{{ asset('theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('theme/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('theme/plugins/fastclick/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('theme/dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('theme/dist/js/demo.js') }}"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('js/report.js') }}"></script>

    <script>



        $(".filter_btn").click(function () {

            var id = $(this).val();


            if (id == 1) {

                $(".search_filter").html('<div class="row"><div class="col-md-4"><form action="#" method="get" ><div class="input-group"><select class="form-control" id="task" name="task"><option  value="Mike11">Mike11</option><option  value="traertyyt">traertyyt</option><option  value="test1234">test1234</option><option  value="aaaaaaaaaaaa">aaaaaaaaaaaa</option><option  value="Testing name 1">Testing name 1</option></select><input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj"><span class="input-group-btn"><button type="submit" name="task" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button><a href="" class="btn btn-danger btn-flat cancel_btn">Cancel</a></span></div></form></div></div>');
            }

            else if (id == 2) {

                $(".search_filter").html('<div class="row"><div style="padding-right:0px" class="col-md-3"><div><select class="form-control" id="category" name="category" placeholder="select category"><option value="Support">Support</option><option value="ahmed">ahmed</option><option value="hany">hany</option><option value="test category">test category</option><option value="Thamer">Thamer</option><option value="Test1">Test1</option></select></div></div><button type="submit" name="category" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button><a href="" class="btn btn-danger btn-flat cancel_btn">Cancel</a></div>');
            }
            else if (id == 3) {

                $(".search_filter").html('<div class="row"><div class="col-md-4"><form action="#" method="get" ><div class="input-group"><input type="date" name="date" class="form-control" placeholder="mm/dd/yyyy"><input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj"><span class="input-group-btn"><button type="submit" name="date" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button><a href="" class="btn btn-danger btn-flat cancel_btn">Cancel</a></span></div></form></div></div>');
            }

            else if (id == 4) {

                $(".search_filter").html('<div class="row"><div style="padding-right:0px" class="col-md-3"> <div><select class="form-control" id="status" name="status"><option value="" selected="selected">Select status</option><option value="Closed">Close</option><option value="Opened">Open</option><option value="Completed">Completed</option></select></div></div><button type="submit" name="status" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button><a href="" class="btn btn-danger btn-flat cancel_btn">Cancel</a></div>');
            }

            else if (id == 5) {

                $(".search_filter").html('<div class="row"><div class="col-md-4"><form action="#" method="get" ><div class="input-group"><select class="form-control" id="client" name="client"><option  value="Awlad de leo">Awlad de leo</option><option  value="Hayk">Hayk</option><option  value="Ahui">Ahui</option><option  value="Test">Test</option></select><input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj"><span class="input-group-btn"><button type="submit" name="client" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button><a href="" class="btn btn-danger btn-flat cancel_btn">Cancel</a></span></div></form></div></div>');
            }
            else if (id == 6) {

                $(".search_filter").html('<div class="row"><div class="col-md-4"><form action="#" method="get" ><div class="input-group"><input  type="number" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" id="pri" name="priority" class="form-control" value="" maxlength="1" min="1" max="5" placeholder="1 - 5"><input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj"><span class="input-group-btn"><button type="submit" name="priority" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button><a href="" class="btn btn-danger btn-flat cancel_btn">Cancel</a></span></div></form></div></div>');
            }

        });


        function maxLengthCheck(object) {
            if (object.value.length > object.maxLength)
                object.value = object.value.slice(0, object.maxLength)
        }

        function isNumeric(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[1-5]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


    </script>

    <script>
function myFunction() {
    window.print();
}
</script>

    @endsection

