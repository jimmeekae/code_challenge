
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var cls='';
    if(user_role=='user'){
            cls='hidden';
        }

var page = 1;
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: "category",
        data: {page: page}
    }).done(function (data) {

        total_page = data.last_page;
        current_page = data.current_page;
        if (total_page > 1) {
            $('#pagination').twbsPagination({
                totalPages: total_page,
                visiblePages: current_page,
                onPageClick: function (event, pageL) {
                    //alert('dataa '+data.data[1].title);
                    page = pageL;

                    if (is_ajax_fire != 0) {
                        //alert('inside');
                        getPageData();
                    }
                }
            });
        }


        manageRow(data.data);

        is_ajax_fire = 1;
    });
}


/* Get Page Data*/
function getPageData() {
    $.ajax({
        dataType: 'json',
        url: "category",
        data: {page: page}
    }).done(function (data) {
        manageRow(data.data);
    });
}

/* Add new Item table row */
function manageRow(data) {
    //alert('inside row'+data);
    var rows = '';
    $.each(data, function (key, value) {
        rows += '<tr><td>' + value.name + '</td><td>' + value.description + '</td><td><div class="btn-group btn-group-xs"> <button value="' + value.id + '" data-toggle="modal" data-target="#edit-category" class="btn disabled btn-info btn-xs btn-flat edit-category '+cls+'"><i class="fa fa-edit"></i> edit</button> <button value="' + value.id + '" data-toggle="modal" data-target="#delete-category" class="btn disabled btn-danger btn-flat btn-xs delete-category '+cls+'"><i class="fa fa-trash"></i> delete</button></td></tr>';
    });

    $("tbody").html(rows);

}

/* Create new Item */
$(".crud-submit-add").click(function (e) {

    e.preventDefault();
    $('.crud-submit-add').attr("disabled", true);
    var form_action = $("#create-category").find("form").attr("action");
    var name = $("#create-category").find("input[name='name']").val();
    var description = $("#create-category").find("textarea[name='details']").val();
    //  alert('create');

    $.ajax({
        dataType: 'json',
        type: 'POST',
        url: form_action,
        data: {
            name: name,
            details: description

        }
    }).done(function (data) {

        $("#create-category").find("input[name='name']").val("");
        $("#create-category").find("textarea[name='details']").val("");
        $("#name_error").hide();

        manageData();
        //getPageData();

        $(".modal").modal('hide');

        toastr.success('Category Added Successfully.', 'Success Alert', {timeOut: 1000});
        $('.crud-submit-add').attr("disabled", false);
    })

        .fail(function (sss) {
            var errors = sss.responseJSON;
            errors=errors.errors;
            $.each(errors, function (index, value) {
                $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i> ' + value + ' </P>');
                $("#name_error").show();
            });

            $('.crud-submit-add').attr("disabled", false);


        });

});

$(".close_btn").click(function () {
    $("#name_error").hide();
    $("#create-category").find("input[name='name']").val("");
    $("#create-category").find("textarea[name='details']").val("");
});

/* Edit Item */
$("#table_category").on("click", ".edit-category", function () {

    var id = $(this).val();

    $.ajax({
        url: "editcategory",
        type: "post",
        data: {
            'id': id
        },
        success: function (s) {
            $("#edit-category").find("input[name='name']").val(s.name);
            $("#edit-category").find("textarea[name='details']").val(s.description);
            $("#edit-category").find("input[name='task']").val(s.id);

        }
    });
});

/* Updated new Item */
$(".crud-submit-edit").click(function (e) {

    e.preventDefault();
    $('.crud-submit-edit').attr("disabled", true);
    var id = $("#edit-category").find("input[name='task']").val();
    // alert(id);

    var form_action = $("#edit-category").find("form").attr("action");
    var name = $("#edit-category").find("input[name='name']").val();
    var details = $("#edit-category").find("textarea[name='details']").val();


    $.ajax({
        dataType: 'json',
        type: 'post',
        url: form_action,
        data: {
            id: id,
            name: name,
            details: details
        }
    }).done(function (data) {
        getPageData();
        $(".modal").modal('hide');
        toastr.success('Category Updated Successfully.', 'Success Alert', {timeOut: 3000});
        $('.crud-submit-edit').attr("disabled", false);
    });

});

/* Remove Item */
$("#table_category").on("click", ".delete-category", function () {
    var id = $(this).val();

    $('#delete-category').find("input[name='id']").val(id);

});

$(".crud-submit-delete").click(function () {
    $('.crud-submit-delete').attr("disabled", true);

    var id= $('#delete-category').find("input[name='id']").val();
    var token= $('#delete-category').find("input[name='_token']").val();
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: "deletecategoty",
        data: {
            id: id,
            _token:token
        },
    }).done(function (data) {
        getPageData();
        $(".modal").modal('hide');
        toastr.success('Category Deleted Successfully.', 'Success Alert', {timeOut: 3000});
        $('.crud-submit-delete').attr("disabled", false);

    });

});
