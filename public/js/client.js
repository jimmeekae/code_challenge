
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var page = 1;
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;

manageData();

/* manage data list */
function manageData() {

    $.ajax({
        dataType: 'json',
        url: "client",
        data: {page: page}
    }).done(function (data) {

        total_page = data.last_page;
        current_page = data.current_page;
        if (total_page > 1) {
            $('#pagination').twbsPagination({
                totalPages: total_page,
                visiblePages: current_page,
                onPageClick: function (event, pageL) {
                    //alert('dataa '+data.data[1].title);
                    page = pageL;

                    if (is_ajax_fire != 0) {
                        //alert('inside');
                        getPageData();
                    }
                }
            });
        }


        manageRow(data.data);

        is_ajax_fire = 1;
    });
}


/* Get Page Data*/
function getPageData() {
    $.ajax({
        dataType: 'json',
        url:"client",
        data: {page: page}
    }).done(function (data) {
        console.log(data);
        manageRow(data.data);
    });
}

/* Add new Item table row */
function manageRow(data) {
    //alert('inside row'+data);
    if(user_role=='admin')
    {
        var cls='';
    }else {
        var cls='hidden';
    }
    var rows = '';
    
    $.each(data, function (key, value) {
        var name = value.user_id;
        var id=value.id;
        var clss='';
         if(id==1 || id==2)
           {
               //alert(id);
               var clss='disabled';
           }
        if (value.roles == 'admin') {
            rows += '<tr><td>' + value.name + '</td><td>' + value.id + '</td><td>' + value.position + '</td><td>' + value.role + '</td><td><div class="btn-group btn-group-xs"><div class="btn-group btn-group-xs"><form action="user_profile" method="get"><input name="prf" id="prf" type="hidden" value="' + value.id + '">'+'<button type="submit" class="btn btn-primary disabled btn-xs btn-flat profile_btn"><i class="fa fa-user"></i> profile</button></form></div><div class="btn-group btn-group-xs"><form action="user_task" method="get"><input name="user" id="user" type="hidden" value="' + value.id + '"><input type="hidden" name="_token" value="{{ csrf_token() }}" /> <button type="submit" class="btn btn-warning disabled btn-xs btn-flat task_btn"><i class="fa fa-edit"></i> task</button></form></div><button value="' + value.id + '" data-toggle="modal" data-target="#edit-client"  class="btn '+cls+' '+clss+' btn-info disabled btn-xs btn-flat edit-client"><i class="fa fa-edit"></i> edit</button><button value="' + value.id + '" data-toggle="modal" data-target="#delete-client"  class="btn '+cls+' '+clss+' btn-danger disabled btn-xs btn-flat delete-client"><i class="fa fa-trash"></i> delete</button></div></td></tr>';

        }

        else {
            rows += '<tr><td>' + value.name + '</td><td>' + value.id + '</td><td>' + value.position + '</td><td>' + value.role + '</td><td><div class="btn-group btn-group-xs"><div class="btn-group btn-group-xs"><form action="user_profile" method="get"><input name="prf" id="prf" type="hidden" value="' + value.id + '">'+'<button type="submit" class="btn btn-primary disabled btn-xs btn-flat profile_btn"><i class="fa fa-user"></i> profile</button></form></div><div class="btn-group btn-group-xs"><form action="user_task" method="get"><input name="user" id="user" type="hidden" value="' + value.id + '"><input type="hidden" name="_token" value="{{ csrf_token() }}" /><button type="submit" class="btn btn-warning disabled btn-xs btn-flat task_btn"><i class="fa fa-edit"></i> task</button></form></div><button value="' + value.id + '" data-toggle="modal" data-target="#edit-client" class="btn '+cls+' '+clss+' btn-info disabled btn-xs btn-flat edit-client"><i class="fa fa-edit"></i> edit</button><button value="' + value.id + '" data-toggle="modal" data-target="#delete-client" class="btn '+cls+' '+clss+' btn-danger disabled btn-xs btn-flat delete-client"><i class="fa fa-trash"></i> delete</button></div></td></tr>';
        }
    });
    //  $("tfoot").html('<tr><th>Total Clients '+data.length+'</th></tr>');

    $("tbody").html(rows);

}


/* Create new Item */
$(".crud-submit-add").click(function (e) {

    e.preventDefault();

    $('.crud-submit-add').attr("disabled", true);
    var form_action = $("#add-member").find("form").attr("action");
    var fname = $("#add-member").find("input[name='fname']").val();
    var lname = $("#add-member").find("input[name='lname']").val();
    var email = $("#add-member").find("input[name='email']").val();
    var position = $("#add-member").find("input[name='position']").val();
    var user_id = $("#add-member").find("input[name='user_id']").val();
    var role = $("#add-member").find("select[name='role']").val();
    var sex = $("#add-member").find("select[name='sex']").val();
    var password = $("#add-member").find("input[name='password']").val();
    var rpassword = $("#add-member").find("input[name='rpassword']").val();


    $.ajax({
        dataType: 'json',
        type: 'post',
        url: form_action,
        data: {
            fname: fname,
            lname: lname,
            email: email,
            position: position,
            user_id: user_id,
            role: role,
            sex: sex,
            password: password,
            rpassword: rpassword,
        }
    }).done(function (data) {

        $("#add-member").find("input[name='fname']").val("");
        $("#add-member").find("input[name='lname']").val("");
        $("#add-member").find("input[name='email']").val("");
        $("#add-member").find("input[name='position']").val("");
        $("#add-member").find("input[name='user_id']").val("");
        $("#add-member").find("select[name='role']").val("");
        $("#add-member").find("select[name='sex']").val("");
        $("#add-member").find("input[name='password']").val("");
        $("#add-member").find("input[name='rpassword']").val("");
        $("#name_error").hide();

        manageData();

        $(".modal").modal('hide');

        toastr.success('Member Added Successfully.', 'Success Alert', {timeOut: 3000});
        $('.crud-submit-add').attr("disabled", false);
    })
        .fail(function (sss) {

            var errors = sss.responseJSON;

            var array = [];
            var i = 0;

            $.each(errors, function (index, value) {
                array[i++] = value;

            });

            if (array[0]) {
                $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i> ' + array[0] + ' </P>');
                $("#name_error").show();
            }

            else {
                $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i> Unique Email or User id is required </P>');
                $("#name_error").show();
            }

            $('.crud-submit-add').attr("disabled", false);

        });

});

$(".close_btn").click(function () {
    $("#name_error").hide();
    $("#add-member").find("input[name='fname']").val("");
    $("#add-member").find("input[name='lname']").val("");
    $("#add-member").find("input[name='email']").val("");
    $("#add-member").find("input[name='position']").val("");
    $("#add-member").find("input[name='user_id']").val("");
    $("#add-member").find("select[name='role']").val("");
    $("#add-member").find("select[name='sex']").val("");
    $("#add-member").find("input[name='password']").val("");
    $("#add-member").find("input[name='rpassword']").val("");

});

/* Edit Item */
$("#table_client").on("click", ".edit-client", function () {
    //alert('edit');
    var id = $(this).val();

    $.ajax({

        url: "editclient",
        type: "post",
        data: {
            'id': id
        },
        success: function (s) {
            //alert(s['user'].email);

            $("#edit-client").find("input[name='email']").val(s['user'].email);
            $("#edit-client").find("input[name='position']").val(s['post'].position);
            $("#edit-client").find("input[name='user_id']").val(s['post'].user_id);
            $("#edit-client").find("select[name='role']").val(s['post'].roles);
            $("#edit-client").find("input[name='client']").val(s['post'].id);

        }
    });
});

/* Updated new Item */
$(".crud-submit-edit").click(function (e) {

    e.preventDefault();
    $('.crud-submit-edit').attr("disabled", true);
    var id = $("#edit-client").find("input[name='client']").val();
    // alert(id);

    var form_action = $("#edit-client").find("form").attr("action");
    var email = $("#edit-client").find("input[name='email']").val();
    var position = $("#edit-client").find("input[name='position']").val();
    var user_id = $("#edit-client").find("input[name='user_id']").val();
    var role = $("#edit-client").find("select[name='role']").val();
    var id = $("#edit-client").find("input[name='client']").val();

    //alert(role);


    $.ajax({
        dataType: 'json',
        type: 'post',
        url: form_action,
        data: {
            id: id,
            email: email,
            position: position,
            role: role,
            user_id: user_id,
        }
    }).done(function (data) {
        getPageData();
        $(".modal").modal('hide');
        toastr.success('Info Updated Successfully.', 'Success Alert', {timeOut: 3000});
        $('.crud-submit-edit').attr("disabled", false);
    });

});

/* Remove Item */
$("#table_client").on("click", ".delete-client", function () {
    var id = $(this).val();
    $('#delete-client').find("input[name='id']").val(id);

});

$(".crud-submit-delete").click(function () {

    $('.crud-submit-delete').attr("disabled", true);

    var id= $('#delete-client').find("input[name='id']").val();
    var token= $('#delete-client').find("input[name='_token']").val();
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: "deleteclient",
        data: {
            id: id,
            _token:token
        }
    }).done(function (data) {
        getPageData();
        $(".modal").modal('hide');
        toastr.success('Member Deleted Successfully.', 'Success Alert', {timeOut: 3000});
        $('.crud-submit-delete').attr("disabled", false);
    });

});
