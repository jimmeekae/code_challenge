$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


var page = 1;
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: "item-task",
        data: {
            page: page
        }
    }).done(function (data) {


        total_page = data.last_page;
        current_page = data.current_page;

        if (total_page > 1) {
            $('#pagination').twbsPagination({
                totalPages: total_page,
                visiblePages: current_page,
                onPageClick: function (event, pageL) {
                    //alert('dataa '+data.data[1].title);
                    page = pageL;

                    if (is_ajax_fire != 0) {
                        //alert('inside');
                        getPageData();
                    }
                }
            });
        }


        manageRow(data.data);

        is_ajax_fire = 1;
    });
}


/* Get Page Data*/
function getPageData() {
    //alert(url);
    $.ajax({
        dataType: 'json',
        url: "item-task",
        data: {page: page}
    }).done(function (data) {
        manageRow(data.data);
    });
}

/* Add new Item table row */
function manageRow(data) {
    //alert('inside row'+data);
    var rows = '';
    var rowsfooter = '';
    var o = 0, c = 0, cm = 0;
    $.each(data, function (key, value) {

        var a = 5 - value.priority_id;
        var category=value.category;
        var department=value.department;
        var access_type=value.access_type;

        rows += '<tr><td>' + value.task_name + '</td><td>' + category.name + '</td><td>' + value.due_date +  '</td><td>' + access_type.name  + '</td><td>' + value.status.name  + '</td><td>' + value.assigned_by.name + '</td><td>' + value.assigned_to.name + '</td><td>' + value.percentage_done  + '</td><td><i class="fa fa-battery-' + a + '"></td><td><div class="btn-group btn-group-xs "><button value="' + value.id + '" data-toggle="modal" data-target="#details-item" class="btn btn-primary btn-flat btn-sm details-task"><i class="fa fa-file"> details</i></button> <button value="' + value.id + '" data-toggle="modal" data-target="#edit-item" class="btn btn-info btn-sm btn-flat edit-task"><i class="fa fa-edit"> edit</i></button> <button value="' + value.id + '" data-toggle="modal" data-target="#delete-item" class="btn btn-danger btn-sm btn-flat delete-task"><i class="fa fa-trash"> delete</i></div></td></tr>';
         console.log(value.status.name);
        if (value.status.name == 'opened') {
            o++
        }
        else if (value.status.name == 'closed') {
            c++
        }
        else if (value.status.name == 'Completed') {
            cm++
        }
    });

    rowsfooter += '<tr><th>Total Task ' + data.length + ' </th><th>Open Task ' + o + ' </th><th>Close Task ' + c + ' </th><th>Complete Task ' + cm + ' </th></tr>';

    $("tbody").html(rows);
    $("tfoot").html(rowsfooter);


}

var expanded = false;
/* show checkboxes when tagging users */
    function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


/* Create new Item */
$(".crud-submit-add").click(function (e) {

    e.preventDefault();

    var full_value = $("#create-item").find("select[name='client_id']").val();

    if(full_value=='' || full_value==null){

        var client_id = null;
        var client_name = null;
    }
    else{
        var full = full_value.split(',');
        var client_id = full[0];
        var client_name = full[1];
    }

    $('.crud-submit-add').attr("disabled", true);
        var form_action = $("#create-item").find("form").attr("action");
        var name = $("#create-item").find("input[name='name']").val();
        var category = $("#create-item").find("select[name='category']").val();
        var status = $("#create-item").find("select[name='status']").val();
        var start_time = $("#create-item").find("input[name='start_date']").val();
        var end_time = $("#create-item").find("input[name='end_date']").val();        
        var priority = $("#create-item").find("input[name='pri']").val();
        var assign = $("#create-item").find("select[name='client_id']").val();
        var access = $("#create-item").find("select[name='access']").val();
        var done = $("#create-item").find("select[name='perc_done']").val();

        var taged_users = [];
            $.each($("input[name='tag_user']:checked"), function(){            
                taged_users.push($(this).val());
            });

        var description = $("#create-item").find("textarea[name='description']").val();

        console.log(taged_users);


        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: form_action,
            data: {
                name: name,
                category: category,
                description: description,
                start_time: start_time,
                end_time: end_time,
                status: status,
                priority: priority,
                assign:assign,
                access:access,
                done:done,
                taged_users:taged_users

            }
        }).done(function (data) {

            $("#create-item").find("input[name='name']").val("");
            $("#create-item").find("select[name='category']").val("");
            $("#create-item").find("select[name='status']").val("");
            $("#create-item").find("input[name='start_date']").val("");
            $("#create-item").find("input[name='end_date']").val("");        
            $("#create-item").find("input[name='pri']").val("");
        $("#create-item").find("select[name='client_id']").val("");
        $("#create-item").find("select[name='access']").val("");
        $("#create-item").find("select[name='perc_done']").val("");


        


            $("#name_error").hide();
            manageData();

            $(".modal").modal('hide');
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 3000});
            $('.crud-submit-add').attr("disabled", false);

        })

            .fail(function (sss) {

                var errors = sss.responseJSON;
                errors=errors.errors;
                 
                var array = [];
                var i = 0;
                $.each(errors, function (index, value) {
                    array[i++] = value;
                });

                $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i>  ' + array[0] + ' </P>');
                $("#name_error").show();
                $('.crud-submit-add').attr("disabled", false);

            });


});

$(".close_btn").click(function () {
    $("#name_error").hide();

    $("#create-item").find("select[name='name']").val("");
    $("#create-item").find("input[name='start_date']").val("");
    $("#create-item").find("input[name='end_date']").val("");
    $("#create-item").find("select[name='category']").val("");
    $("#create-item").find("select[name='client_id']").val("");
    $("#create-item").find("select[name='status']").val("");
    $("#create-item").find("input[name='pri']").val("");
    $("#create-item").find("textarea[name='description']").val("");
});


/* Edit Item */
$("#table_task").on("click", ".edit-task", function () {
    var id = $(this).val();
    // alert(id);

    $.ajax({

        url: "edittask",
        type: "post",
        data: {
            'id': id
        },
        success: function (s) {
            console.log(s.category.name);

            $("#edit-item").find("input[name='name']").val(s.task_name);
            $("#edit-item").find("input[name='start_date']").val(s.start_date);
            $("#edit-item").find("select[name='category']").val(s.category_id);
            $("#edit-item").find("input[name='end_date']").val(s.due_date);
            $("#edit-item").find("select[name='client_id']").val(s.assigned_to_id);
            $("#edit-item").find("select[name='status']").val(s.status_id);
            $("#edit-item").find("select[name='access']").val(s.access_type_id);
            $("#edit-item").find("select[name='perc_done']").val(s.percentage_done);
            $("#edit-item").find("input[name='pri']").val(s.priority_id);
            $("#edit-item").find("textarea[name='description']").val(s.task_description);
            $("#edit-item").find("input[name='task']").val(s.id);

        }
    });
});

/* Updated new Item */
$(".crud-submit-edit").click(function (e) {

    e.preventDefault();

    var full_value = $("#edit-item").find("select[name='client_id']").val();

    if(full_value=='' || full_value==null){

        var client_id = null;
        var client_name = null;
    }
    else{
        var full = full_value.split(',');
        var client_id = full[0];
        var client_name = full[1];
    }

    $('.crud-submit-edit').attr("disabled", true);

        var id = $("#edit-item").find("input[name='task']").val();
        var form_action = $("#edit-item").find("form").attr("action");
        var name = $("#edit-item").find("input[name='name']").val();
        var category = $("#edit-item").find("select[name='category']").val();
        var status = $("#edit-item").find("select[name='status']").val();
        var start_time = $("#edit-item").find("input[name='start_date']").val();
        var end_time = $("#edit-item").find("input[name='end_date']").val();        
        var priority = $("#edit-item").find("input[name='pri']").val();
        var assign = $("#edit-item").find("select[name='client_id']").val();
        var access = $("#edit-item").find("select[name='access']").val();
        var done = $("#edit-item").find("select[name='perc_done']").val();
        var description = $("#edit-item").find("textarea[name='description']").val();


        $.ajax({
            dataType: 'json',
            type: 'post',
            url: form_action,
            data: {
                id: id,
                name: name,
                category: category,
                description: description,
                start_time: start_time,
                end_time: end_time,
                status: status,
                priority: priority,
                assign:assign,
                access:access,
                done:done
            }
        }).done(function (data) {
            getPageData();
            $(".modal").modal('hide');

            toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 3000});
            $('.crud-submit-edit').attr("disabled", false);
        })

            .fail(function (sss) {

                var errors = sss.responseJSON;

                var array = [];
                var i = 0;
                $.each(errors, function (index, value) {
                    array[i++] = value;
                });

                $("#edit_error").html('<p><i class="fa fa-exclamation-triangle"></i>  ' + array[0] + ' </P>');
                $("#edit_error").show();
                $('.crud-submit-edit').attr("disabled", false);
            });




});

/* Remove Item */
$("#table_task").on("click", ".delete-task", function () {
    var id = $(this).val();
    $('#delete-item').find("input[name='id']").val(id);
    // alert(id);

});

$(".crud-submit-delete").click(function () {

    $('.crud-submit-delete').attr("disabled", true);

    var id= $('#delete-item').find("input[name='id']").val();
    var token= $('#delete-item').find("input[name='_token']").val();

    $.ajax({
        dataType: 'json',
        type: 'post',
        url: "deletetask",
        data: {
            id: id
        }
    }).done(function (data) {
        getPageData();
        $(".modal").modal('hide');
        toastr.error('Item Deleted Successfully.', 'Success Alert', {timeOut: 3000});
        $('.crud-submit-delete').attr("disabled", false);


    });

});

//details modal
$("#table_task").on("click", ".details-task", function () {
    var id = $(this).val();
    // alert(id);

    $.ajax({

        url: "edittask",
        type: "post",
        data: {
            'id': id
        },
        success: function (s) {
        console.log(s);
            $("h5").html(s.task_description);
            // $("#edit").find("textarea[name='description']").val();

        }
    });
});


$(".search_part").on("click", ".cancel_btn", function (e) {
    e.preventDefault();
    // alert('yes');
    $(".search_filter").html("");
    getPageData();

});

$(".search_part").on("click", "#search-btn", function (e) {
    e.preventDefault();
    var element = $(this);
    var name = element.attr("name");

    if (name == 'task') {
        var data = $(".search_part").find("select[name='task']").val();
    }
    else if (name == 'category') {
        var data = $(".search_part").find("select[name='category']").val();
    }
    else if (name == 'date') {
        var data = $(".search_part").find("input[name='date']").val();
    }

    else if (name == 'status') {
        var data = $(".search_part").find("select[name='status']").val();
    }
    else if (name == 'client') {
        var data = $(".search_part").find("select[name='client']").val();
    }
    else if (name == 'priority') {
        var data = $(".search_part").find("input[name='priority']").val();
    }

    $.ajax({
        type: "post",
        url: "gettask",
        data: {
            name: name,
            data: data
        },
        success: function (s) {
            $("tbody").html("");
            $("tfoot").html("");
            var rows1 = '';
            var rowsfooter = '';
            $.each(s, function (key, value) {
               var rowsfooter = '';
                var a = 5 - value.priority_id;

                var category=value.category;
        var department=value.department;
        var access_type=value.access_type;

        rows1 += '<tr><td>' + value.task_name + '</td><td>' + category.name + '</td><td>' + value.due_date +  '</td><td>' + access_type.name  + '</td><td>' + value.status.name  + '</td><td>' + value.assigned_by.name + '</td><td>' + value.assigned_to.name + '</td><td>' + value.percentage_done  + '</td><td><i class="fa fa-battery-' + a + '"></td><td><div class="btn-group btn-group-xs "><button value="' + value.id + '" data-toggle="modal" data-target="#details-item" class="btn btn-primary btn-flat btn-sm details-task"><i class="fa fa-file"> details</i></button> <button value="' + value.id + '" data-toggle="modal" data-target="#edit-item" class="btn btn-info btn-sm btn-flat edit-task"><i class="fa fa-edit"> edit</i></button> <button value="' + value.id + '" data-toggle="modal" data-target="#delete-item" class="btn btn-danger btn-sm btn-flat delete-task"><i class="fa fa-trash"> delete</i></div></td></tr>';
            });

            $("tbody").html(rows1);
            $('#pagination').html(" ");
            rowsfooter += '<tr><th>Total ' + s.length + ' </th></tr>';
            $("tfoot").html(rowsfooter);
            

        }
    });

});
