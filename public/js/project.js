$(document).ready(function(){

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    var cls='';
    if(user_role=='user'){
            cls='hidden';
        }
    var page = 1;
    var current_page = 1;
    var total_page = 0;
    var is_ajax_fire = 0;

    manageData();

    /* manage data list */
    function manageData() {
        $.ajax({
            type:'post',
            dataType: 'json',
            url: "projectdata",
            data: {page: page}
        }).done(function (data) {
            total_page = data.last_page;
            current_page = data.current_page;
            if (total_page > 1) {
                $('#pagination').twbsPagination({
                    totalPages: total_page,
                    visiblePages: current_page,
                    onPageClick: function (event, pageL) {
                        //alert('dataa '+data.data[1].title);
                        page = pageL;

                        if (is_ajax_fire != 0) {
                            //alert('inside');
                            getPageData();
                        }
                    }
                });
            }


            manageRow(data.data);

            is_ajax_fire = 1;
        });
    }


    /* Get Page Data*/
    function getPageData() {
        $.ajax({
            type:'post',
            dataType: 'json',
            url: 'projectdata',
            data: {page: page}
        }).done(function (data) {
            manageRow(data.data);
        });
    }

    /* Add new Item table row */
    function manageRow(data) {

        var rows = '';
       
        $.each(data, function (key, value) {

            rows += '<tr><td>' + value.name + '</td><td>' + value.leader + '</td><td>' + value.status + '</td><td><div class="btn-group btn-group-xs"><div class="btn-group btn-group-xs"><button value="'+value.details+'" type="submit" class="btn btn-primary btn-xs btn-flat details_btn"><i class="fa fa-user"></i> details</button></div><button value="' +value.id+ '" data-toggle="modal" data-target="#edit-project"  class="btn btn-info btn-xs btn-flat edit-project '+cls+'"><i class="fa fa-edit"></i> edit</button><button value="'+value.id+'" data-toggle="modal" data-target="#delete-project"  class="btn btn-danger btn-xs btn-flat delete_project '+cls+'"><i class="fa fa-trash"></i> delete</button></div></td></tr>';
        });

        $("tbody").html(rows);

    }



    $('#create-project').on('click','.crud-submit-add',function (e) {
        e.preventDefault();

        var name=$('#create-project').find("input[name='name']").val();
        var leader=$('#create-project').find("select[name='leader']").val();
        var status=$('#create-project').find("select[name='status']").val();
        var token=$('#create-project').find("input[name='_token']").val();
        var details=$('#create-project').find("textarea[name='details']").val();



        if(name.length<6 || name==''){
            $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i> Name atleast 6 charecter required. </P>');
            $("#name_error").show();
        }
        else if(leader==null || leader==''){
            $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i> Team Leader required. </P>');
            $("#name_error").show();
        }
        else if(status==null || status==''){
            $("#name_error").html('<p><i class="fa fa-exclamation-triangle"></i> Status required. </P>');
            $("#name_error").show();
        }
        else{
            var full=leader.split(',');
            var leader_id=full[0];
            var leader_name=full[1];
            $('.crud-submit-add').attr("disabled", true);

            $.ajax({
                dataType: 'json',
                type:'POST',
                url: "projects",
                data:{
                    name:name,
                    leader_id:leader_id,
                    leader_name:leader_name,
                    details:details,
                    status:status,
                    token:token
                }
            }).done(function(data){

                reset();
                $(".modal").modal('hide');
                toastr.success('Project Added Successfully.', 'Success Alert', {timeOut: 3000});
                manageData();
                $('.crud-submit-add').attr("disabled", false);
            })

                .fail(function(sss){
                    $("#name_error").hide();
                    $(".modal").modal('hide');
                    toastr.error('Something goes Wrong.', 'Error Alert', {timeOut: 3000});
                    $('.crud-submit-add').attr("disabled", false);
                });

        }



    });


    $('table').on('click','.details_btn',function () {
        var details=$(this).val();

        $("#details-project").modal('show');
        $("h5").html(details);
    });



    $('table').on('click','.delete_project',function () {

        var project_id=$(this).val();
        $('#delete-project').find("input[name='id']").val(project_id);


    });


    $('#delete-project').on('click','.crud-submit-delete',function () {

        $('.crud-submit-delete').attr("disabled", true);
        var id= $('#delete-project').find("input[name='id']").val();
        var token= $('#delete-project').find("input[name='_token']").val();

        $.ajax({
            dataType: 'json',
            type:'POST',
            url: "projects_delete",
            data:{
                id:id,
                _token:token
            }
        }).done(function(data){
            $('#delete-project').modal('hide');
            manageData();
            toastr.success('Project Deleted Successfully.', 'Success Alert', {timeOut: 3000});
            $('.crud-submit-delete').attr("disabled", false);
        })

            .fail(function(sss){
                $('#delete-project').modal('hide');
                toastr.error('Something goes Wrong.', 'Error Alert', {timeOut: 3000});
                $('.crud-submit-delete').attr("disabled", false);
            });

    });

    $('table').on('click','.edit-project',function () {
        var id=$(this).val();
        $("#edit_error").hide();
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: "edit_data",
            data:{
                id:id
            }
        }).done(function(data){
            $('#edit-project').find("input[name='name']").val(data.project);
            $('#edit-project').find("select[name='leader']").val(data.leader+','+data.leader_id);
            $('#edit-project').find("select[name='status']").val(data.status);
            $('#edit-project').find("input[name='id']").val(data.id);
            $('#edit-project').find("textarea[name='details']").val(data.details);
        })

    });

    $('#edit-project').on('click','.crud-submit-edit',function (e) {
        e.preventDefault();

        var id=$('#edit-project').find("input[name='id']").val();
        var name=$('#edit-project').find("input[name='name']").val();
        var leader=$('#edit-project').find("select[name='leader']").val();
        var status=$('#edit-project').find("select[name='status']").val();
        var token=$('#edit-project').find("input[name='_token']").val();
        var details=$('#edit-project').find("textarea[name='details']").val();

        if(name.length<6 || name==''){
            $("#edit_error").html('<p><i class="fa fa-exclamation-triangle"></i> Name atleast 6 charecter required. </P>');
            $("#edit_error").show();
        }
        else if(leader==null || leader==''){
            $("#edit_error").html('<p><i class="fa fa-exclamation-triangle"></i> Team Leader required. </P>');
            $("#edit_error").show();
        }
        else if(status==null || status==''){
            $("#edit_error").html('<p><i class="fa fa-exclamation-triangle"></i> Status required. </P>');
            $("#edit_error").show();
        }

        else{
            var full=leader.split(',');
            var leader_id=full[1];
            var leader_name=full[0];
            $('.crud-submit-edit').attr("disabled", true);

            $.ajax({
                dataType: 'json',
                type:'POST',
                url: "update_project",
                data:{
                    id:id,
                    name:name,
                    leader_id:leader_id,
                    leader_name:leader_name,
                    details:details,
                    status:status,
                    token:token
                }
            }).done(function(data){

                $("#edit_error").hide();
                $(".modal").modal('hide');
                toastr.success('Project Added Successfully.', 'Success Alert', {timeOut: 3000});
                manageData();
                $('.crud-submit-edit').attr("disabled", false);
            })

                .fail(function(sss){
                    $("#edit_error").hide();
                    $(".modal").modal('hide');
                    toastr.error('Something goes Wrong.', 'Error Alert', {timeOut: 3000});
                    $('.crud-submit-edit').attr("disabled", false);
                });
        }



    });

    $('.close_btn').click(function () {
         reset();
    });

    function reset() {
        $('#create-project').find("input[name='name']").val('');
        $('#create-project').find("select[name='leader']").val('');
        $('#create-project').find("select[name='status']").val('');
        $('#create-project').find("textarea[name='details']").val('');
        $("#name_error").hide();
    }



});