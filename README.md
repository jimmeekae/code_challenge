## How to install

## run `composer install`
## Edit .env to have your database settings
## use mysql dump file in root folder called database_dump.sql as data source.
finally run `php artisan serve`


Credentials for testing the application include-:

admin@demo.com - Manager in the ICT Department.
user@demo.com - Junior employee in the ICT Department.

admin1@demo.com - Manager in the Finance Department.
user1@demo.com - Junior employee in the Finance Department. 

password =123456 for all the accounts.