<?php $__env->startSection('title', 'Tasks : Home'); ?>

<?php $page="departments" ?>



<?php $__env->startSection('header'); ?>
 <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php $__env->stopSection(); ?>
<?php $__env->startSection('sidebar'); ?>
    <?php echo $__env->make('layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <!-- Content Wrapper. Contains page content -->
 <?php $__env->startSection('content'); ?>

 <script src="<?php echo e(asset('theme/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });


    function submitform() {
        document.myform.submit();
    }


    $('.notify').click(function () {
        var id = $(this).attr('id');
        var token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: "post",
            url: "/clear-notification",
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            data: {
                'id': id,
                _token: token
            },
            success: function (s) {


                if (s.status == 'message') {


                    $('#mess').html('<input name="msg" id="msg" type="hidden" value="1">')
                    submitform();
                }
                else {
                    window.location.replace('mytask');

                }
            }
        });
    });
</script>
   
       <!-- Content Wrapper. Contains page content -->

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Admin
                    <small>Control panel</small>
                </h1>

                <div class="table_box">
                    <div class="box-header">
                        <div style="margin-bottom:2em" class="col-md-12">
                        <h3 class="box-title">Category Management</h3>
                        </div>
                        <div class="btn-group btn-group-sm pull-right">
                            <button type="button" class="btn disabled btn-success btn-sm" data-toggle="modal"
                                    data-target="#create-category"><i class="fa fa-plus"></i> Add Category
                            </button>
                        </div>


                        <div class="example-modal modal fade" id="create-category" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-success">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close close_btn" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Add New Category</h4>
                                        </div>
                                        <div style="padding-left:20px;padding-right:20px" class="modal-body">
                                            <form data-toggle="validator" action="http://task.exodias.com/category"
                                                  method="POST">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="name">Category Name *</label>

                                                            <input type="text" id="name" name="name"
                                                                   class="form-control" placeholder="category name">

                                                        </div>

                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <div style="display:none;padding-top:30px"
                                                                 class="form-group" id="name_error"></div>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">Details</label>
                                                            <textarea style="resize:none" name="details" id="details"
                                                                      class="form-control"
                                                                      placeholder="Tell me details about categories in short"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-add">Add
                                                Category
                                            </button>
                                            <button type="submit" class="btn btn-outline close_btn"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- /.example-modal -->

                        <!-- /edit modal-->
                        <div class="example-modal modal fade" id="edit-category" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-primary">
                                <div class="modal-dialog">

                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Edit Category</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form data-toggle="validator"
                                                  action=http://task.exodias.com/updatecategory method="post">
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="name">Category Name *</label>
                                                            <input type="text" id="name" name="name"
                                                                   class="form-control" placeholder="category name">
                                                        </div>
                                                    </div>

                                                    <input name="task" id="task" type="hidden" value="">

                                                    <input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="description">Details</label>
                                                            <textarea name="details" id="details" class="form-control"
                                                                      placeholder="Tell me details about categories "></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-outline pull-left crud-submit-edit">
                                            Update
                                        </button>
                                        <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                        </button>
                                    </div>

                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <!-- end edit modal-->

                        <!-- /delete modal-->
                        <div class="example-modal modal fade" id="delete-category" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal modal-danger">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4>Delete Category</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" id="id" name="id"
                                                   class="form-control hidden">
                                            <input type="text" id="_token" name="_token"
                                                   class="form-control hidden" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">


                                            <p style="text-align: center;">Are you sure you want to delete the category
                                                ?</p>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-outline pull-left crud-submit-delete">
                                                Delete
                                            </button>
                                            <button type="submit" class="btn btn-outline" data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>

                        <!-- end delete modal-->


                    </div>
                    <!-- /.box-header -->
                    <div id="table_category" class="box-body">
                        <table id="example2" class="table table-bordered table-hover task_table">
                            <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Details</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!--content-->

                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <ul id="pagination" class="pagination-sm"></ul>
                    </div>
                    <!-- /.box-body -->
                </div>

            </section>

        </div>
        <!-- /.content-wrapper -->
       

    <?php $__env->startSection('footer'); ?>

    <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php $__env->stopSection(); ?>

    <?php $__env->stopSection(); ?>

    <!-- REQUIRED JS SCRIPTS -->

   <?php $__env->startSection('extra-js'); ?>


 <!-- jQuery 2.2.3 -->
    <script src="<?php echo e(asset('theme/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo e(asset('theme/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <!-- AdminLTE App -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo e(asset('theme/plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo e(asset('theme/plugins/fastclick/fastclick.js')); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo e(asset('theme/dist/js/app.min.js')); ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo e(asset('theme/dist/js/demo.js')); ?>"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script src="<?php echo e(asset('js/department.js')); ?>"></script>


    <?php $__env->stopSection(); ?>


<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>