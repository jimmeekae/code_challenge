<?php $__env->startSection('title', 'Tasks : MyTasks'); ?>

<?php $page="mytask" ?>



<?php $__env->startSection('header'); ?>
 <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <?php $__env->stopSection(); ?>
<?php $__env->startSection('sidebar'); ?>
    <?php echo $__env->make('layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>

    <!-- Content Wrapper. Contains page content -->
 <?php $__env->startSection('content'); ?>

 <script src="<?php echo e(asset('theme/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });


    function submitform() {
        document.myform.submit();
    }


    $('.notify').click(function () {
        var id = $(this).attr('id');
        var token = $('meta[name="_token"]').attr('content');

        $.ajax({
            type: "post",
            url: "/clear-notification",
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            data: {
                'id': id,
                _token: token
            },
            success: function (s) {


                if (s.status == 'message') {


                    $('#mess').html('<input name="msg" id="msg" type="hidden" value="1">')
                    submitform();
                }
                else {
                    window.location.replace('mytask');

                }
            }
        });
    });
</script>
   
       <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
                    <h1 >
                Admin
                <small>Control panel</small>
            </h1>
        


      <div class="table_box">
        <div class="box-header">
          <h3 class="box-title">All Task</h3>

        </div>
        <!-- /.box-header -->

        <div id="task_modal" class="modal-body">
          <div class="tabbable"> <!-- Only required for left/right tabs -->
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab1" data-toggle="tab">All Task</a></li>
              <li><a href="#tab2" data-toggle="tab">Open Task</a></li>
              <li><a href="#tab3" data-toggle="tab">Close Task</a></li>
              <li><a href="#tab4" data-toggle="tab">Complete Task</a></li>
            </ul>
            <div class="tab-content">

              <!--/All Task-->

              <div class="tab-pane active" id="tab1"> 

                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover task_table">
                    <thead>
                      <tr>
                        <th>Task</th>
                        <th>Category</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Priority</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <tr>
                        <td><?php echo e($task->task_name); ?></td>
                        <td><?php echo e($task->category->name); ?></td>
                        <td><?php echo e($task->start_date); ?></td>
                        <td><?php echo e($task->due_date); ?></td>
                        <td><?php echo e($task->status->name); ?></td>

                        <td><i class="fa fa-battery-<?php echo e($task->priority_id); ?>"></i></td>
                        <td> 
                          <div class="btn-group btn-group-xs">
                           <button type="button" class="btn btn-primary btn-xs details" data-toggle="modal" data-target="#task_details" value="<?php echo e($task->id); ?>"><i class="fa fa-file"></i> details</button> 
                         </div>  
                       </td>
                     </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      
               </tbody>
               <tfoot>
                <tr>
                  <th>Total Task <?php echo e($tasks->count()); ?> </th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
          <!--end All Task-->

        </div>

        <div class="tab-pane" id="tab2">
         <!--/All Task-->

         <div class="box-body">
          <table id="example2" class="table table-bordered table-hover task_table">
            <thead>
              <tr>
                <th>Task</th>
                <th>Category</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                <th>Priority</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              
              <?php
               
               $open_tasks=$tasks->where('status_id', 2);
               ?>                                  
                      <?php $__currentLoopData = $open_tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                             
             <tr>
                        <td><?php echo e($task->task_name); ?></td>
                        <td><?php echo e($task->category->name); ?></td>
                        <td><?php echo e($task->start_date); ?></td>
                        <td><?php echo e($task->due_date); ?></td>
                        <td><?php echo e($task->status->name); ?></td>

                        <td><i class="fa fa-battery-<?php echo e($task->priority_id); ?>"></i></td>
                        <td> 
                          <div class="btn-group btn-group-xs">
                           <button type="button" class="btn btn-primary btn-xs details" data-toggle="modal" data-target="#task_details" value="<?php echo e($task->id); ?>"><i class="fa fa-file"></i> details</button> 
                         </div>  
                       </td>
                     </tr>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>       
           </tbody>
           <tfoot>
            <tr>
              <th>Total Open Task <?php echo e($open_tasks->count()); ?> </th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
      <!--end All Task-->
    </div>

    <div class="tab-pane" id="tab3">
      <!--/All Task-->

      <div class="box-body">
        <table id="example2" class="table table-bordered table-hover task_table">
          <thead>
            <tr>
              <th>Task</th>
              <th>Category</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Status</th>
              <th>Priority</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

          <?php
               
               $closed_tasks=$tasks->where('status_id', 1);
               ?>                                  
                      <?php $__currentLoopData = $closed_tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                             
             <tr>
                        <td><?php echo e($task->task_name); ?></td>
                        <td><?php echo e($task->category->name); ?></td>
                        <td><?php echo e($task->start_date); ?></td>
                        <td><?php echo e($task->due_date); ?></td>
                        <td><?php echo e($task->status->name); ?></td>

                        <td><i class="fa fa-battery-<?php echo e($task->priority_id); ?>"></i></td>
                        <td> 
                          <div class="btn-group btn-group-xs">
                           <button type="button" class="btn btn-primary btn-xs details" data-toggle="modal" data-target="#task_details" value="<?php echo e($task->id); ?>"><i class="fa fa-file"></i> details</button> 
                         </div>  
                       </td>
                     </tr>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>       
                        

                                                                                                                               
         </tbody>
         <tfoot>
          <tr>
            <th>Total Close Task <?php echo e($closed_tasks->count()); ?> </th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <!--end All Task-->
  </div>

  <div class="tab-pane" id="tab4">
   <!--/All Task-->

   <div class="box-body">
    <table id="example2" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Task</th>
          <th>Category</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Status</th>
          <th>Priority</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
<?php
               
               $closed_tasks=$tasks->where('status_id', 3);
               ?>                                  
                      <?php $__currentLoopData = $closed_tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                             
             <tr>
                        <td><?php echo e($task->task_name); ?></td>
                        <td><?php echo e($task->category->name); ?></td>
                        <td><?php echo e($task->start_date); ?></td>
                        <td><?php echo e($task->due_date); ?></td>
                        <td><?php echo e($task->status->name); ?></td>

                        <td><i class="fa fa-battery-<?php echo e($task->priority_id); ?>"></i></td>
                        <td> 
                          <div class="btn-group btn-group-xs">
                           <button type="button" class="btn btn-primary btn-xs details" data-toggle="modal" data-target="#task_details" value="<?php echo e($task->id); ?>"><i class="fa fa-file"></i> details</button> 
                         </div>  
                       </td>
                     </tr>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                                                                                                </tbody>
     <tfoot>
      <tr>
        <th>Total Complete Task <?php echo e($closed_tasks->count()); ?></th>
      </tr>
    </tfoot>
  </table>
</div>
<!-- /.box-body -->
<!--end All Task-->
</div>


</div>
</div>
</div>

<!-- /details modal-->
<div class="example-modal modal fade" id="task_details"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal modal-primary">
    <div class="modal-dialog">
      <div class="modal-content">
        <div  class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 >Task Details</h4>
          </div>
          <div  class="modal-body">

           <p id="show" style="color:white;padding-right:3%;padding-left: 3%;font-size: 20px;"></p>
           
         </div>
         <div  class="modal-footer">
          <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div>

<!-- end details modal-->


</div>

</section>

</div>
<!-- /.content-wrapper -->
       

    <?php $__env->startSection('footer'); ?>

    <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php $__env->stopSection(); ?>

    <?php $__env->stopSection(); ?>

    <!-- REQUIRED JS SCRIPTS -->

   <?php $__env->startSection('extra-js'); ?>


<!-- jQuery 2.2.3 -->
<script src="<?php echo e(asset('theme/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo e(asset('theme/bootstrap/js/bootstrap.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo e(asset('theme/plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('theme/plugins/fastclick/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('theme/dist/js/app.min.js')); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('theme/dist/js/demo.js')); ?>"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo e(asset('theme/plugins/fullcalendar/fullcalendar.min.js')); ?>"></script>

<script type="text/javascript">

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $("#task_modal").on("click",".details",function(){
    //alert('edit');
    var id=$(this).val();
    

    $.ajax({

     url: "/edittask",
     type:"post",
     data:{
      'id':id
    },
    success:function(s){

     $("#show").html(s.task_description);
     
   }
 });
    
  });
</script>



    <?php $__env->stopSection(); ?>


<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>