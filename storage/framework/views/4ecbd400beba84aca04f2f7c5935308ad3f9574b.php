<header class="main-header">

    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>T</b>MS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Task</b> Management System</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>


        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- Notifications Menu -->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                                            </a>
                    <ul class="dropdown-menu">
                        <li class="header" style="text-align: center"><i style="padding-right: 1em"
                                                                         class="fa fa-envelope-o"></i> you
                            have 0 new messages
                        </li>

                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                                            <!-- end message -->
                            </ul>
                        </li>

                        <li class="footer"><a href="#"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a></li>
                    </ul>
                </li>

                <li class="dropdown messages-menu">
                <?php $employee_id=Auth::user()->id;
                $notifications=get_notifications($employee_id);
                 $size=sizeof($notifications) 

                ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <?php if($size>0): ?>
                         <span class="label label-warning"><?php echo e($size); ?></span> <?php endif; ?> </a>
                    <ul class="dropdown-menu">
                        <li class="header" style="text-align: center"><i style="padding-right: 1em"
                                                                         class="fa fa-tasks"></i> you
                            have <?php echo e($size); ?> task notifications
                        </li>

                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                   <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <li><!-- start message -->
                                        <a href="#" class="notify" id="<?php echo e($notification->id); ?>">
                                            <div class="pull-left">
                                                <img src="<?php echo e(asset('theme/dist/img/1528135843.jpg')); ?>" class="img-circle"
                                                     alt="User Image">
                                            </div>
                                        
                                                <h4>
                                                <?php echo e($notification->task_name); ?>

                                                <small>
                                                    <i class="fa fa-clock-o"></i> sometime ago
                                                </small>
                                            </h4>
                                                                                            <p><?php echo e($notification->notification); ?></p>
                                                                                    </a>
                                    </li>        

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>               
                                                        <!-- end message -->
                            </ul>
                        </li>

                        <li class="footer"><a href="#"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a></li>
                    </ul>
                </li>

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->

                        <img src="<?php echo e(asset('theme/dist/img/1528135843.jpg')); ?>" class="user-image" alt="User Image">

                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"><?php echo e(Auth::user()->name); ?> </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li  class="user-header">

                            <img src="<?php echo e(asset('theme/dist/img/1528135843.jpg')); ?>" class="img-circle" alt="User Image">


                            <p >

                                <?php echo e(Auth::user()->name); ?> <br>
                                <small>Software Engineer</small>
                                Task Sharing
                                <br>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <form class="profile_form" name="myform" action="#" method="get">
                                    <input name="prf" id="prf" type="hidden" value="1">
                                    <input type="hidden" name="_token" value="MbOjj3w1undRr5woFJ5ND3FgqUX7PrB6qzLPWnZj">
                                    <div id="mess">

                                    </div>

                                    <a href="javascript: submitform()" style="background-color:#00a65a;color:white;"
                                       class="btn btn-flat submit1">Profile</a>
                                </form>

                            </div>
                            <div class="pull-right">
                                                  <a style="background-color:#dd4b39;color: white" href="<?php echo e(route('login')); ?>" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>

                                                     <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>

        </div>
    </nav>
</header>
