<!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title>Task Portal | <?php echo $__env->yieldContent('title', ''); ?></title>

        <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo e(asset('theme/dist/img/fevicon.jpg')); ?>">
    <!-- Bootstrap 3.3.6 -->


    <link rel="stylesheet" href="<?php echo e(asset('theme/bootstrap/css/bootstrap.min.css')); ?>">
    <!-- Font Awesome -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/AdminLTE.min.css')); ?>">
    
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/skins/skin-blue.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/skins/_all-skins.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/stylesheet/style.css')); ?>">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/fullcalendar/fullcalendar.min.css')); ?>">
  
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/AdminLTE.min.css')); ?>">
    <link href = "<?php echo e(asset('css/jquery-ui.css')); ?>">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">