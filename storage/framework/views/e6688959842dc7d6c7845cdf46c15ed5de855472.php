<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title>Task Portal | <?php echo $__env->yieldContent('title', ''); ?></title>

   <?php $__env->startSection('css'); ?>
        <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo e(asset('theme/dist/img/fevicon.jpg')); ?>">
    <!-- Bootstrap 3.3.6 -->


    <link rel="stylesheet" href="<?php echo e(asset('theme/bootstrap/css/bootstrap.min.css')); ?>">
    <!-- Font Awesome -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/AdminLTE.min.css')); ?>">
    
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/skins/skin-blue.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/skins/_all-skins.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('theme/stylesheet/style.css')); ?>">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="<?php echo e(asset('theme/plugins/fullcalendar/fullcalendar.min.css')); ?>">
  
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('theme/dist/css/AdminLTE.min.css')); ?>">
    <link href = "<?php echo e(asset('css/jquery-ui.css')); ?>">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

    <link href = "<?php echo e(asset('js/task.js')); ?>">
    <style type="css">
         td{
    border:1px solid #000;
}

tr td:last-child{
    width:1%;
    white-space:nowrap;
}
    </style>
    
<?php echo $__env->yieldSection(); ?>
       
    
    </head>


<body class="hold-transition skin-blue sidebar-mini">
<?php echo $__env->yieldContent('header'); ?>
<?php echo $__env->yieldContent('sidebar'); ?>

<div class="wrapper">
  
    <?php echo $__env->yieldContent('content'); ?>

    <?php echo $__env->yieldContent('footer'); ?>

    <?php echo $__env->yieldContent('extra-js'); ?>
 </div>   

</body>
</html>
