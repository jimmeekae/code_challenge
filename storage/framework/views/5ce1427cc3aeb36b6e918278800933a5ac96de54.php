<!-- Table content -->
<table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Task key</th>
                  <th>Task Name</th>
                  <th>Task Category</th>
                  <th>Priority</th>
                  <th>Due date</th>
          <th>Status</th>
          <th>% Done</th>
          <th>Created by</th>
          <th>Assigned to</th>
          <th>Comments</th>
          <th>Follow</th>
                </tr>
                </thead>
                <tbody id="table_data">
        
                <?php 
      if($access_id=="0")
      {
        $tasks = DB::select('select * from tasks');
      }else{
        $tasks = DB::select('select * from tasks where access_id = ?',[$access_id]);
      }
          foreach ($tasks as $task) {
      $access_type=$task->access_id;
          $task_key = $task->id;
        $task_name=$task->task_name;
      $task_category=$task->category_id;
      $category=DB::select('select * from categories where id = ? limit 1', [$task_category]);
      $task_category=$category[0]->name;
      
      $task_priority=$task->priority_id;
      $priority=DB::select('select * from priority where id = ? limit 1', [$task_priority]);
      $task_priority=$priority[0]->priority_name;
      
      $due_date=$task->due_date;
      $status=$task->status;
      $perc_done=$task->percentage_done;
      
      $created_by_id=$task->created_by_id;
      $user=DB::select('select * from users where id = ? limit 1', [$created_by_id]);
      $created_by=$user[0]->name;
      
      $assign_to_id=$task->assigned_to_id;
      $user=DB::select('select * from users where id = ? limit 1', [$assign_to_id]);
      $assign_to=$user[0]->name;
      
      $user=DB::select('select * from users where id = ? limit 1', [$user_id]);
      $level_id=$user[0]->level_id;
      if($level_id==1)
      {
          ?>
                <tr>
                  <td><?php echo $task_key ?></td>
                  <td>
          <?php echo $task_name ?>
                  </td>
                  <td><?php echo $task_category ?></td>
                  <td><?php echo $task_priority ?></td>
                  <td><?php echo $due_date ?></td>
          <td style="color: blue;" onclick="show_modal_status(<?php echo $task_key ?>)"><a href="#"><?php echo $status ?></a></td>
                 <td style="color: blue;" onclick="show_modal_done(<?php echo $task_key ?>)"><a href="#"><?php echo $perc_done ?></a></td>
                  <td><?php echo $created_by ?></td>
                  <td style="color: blue;" onclick="show_modal1(<?php echo $assign_to_id ?>,'<?php echo $assign_to ?>')"><a href="#"><?php echo $assign_to ?></a></td>
                  <td><button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalNorm" onclick="assign_task_id(<?php echo $task_key; ?>)">
    view/add
</button></td>
                  <td><input type="checkbox" id="follow_task<?php echo $task_key; ?>"  onchange="follow_task(<?php echo $task_key; ?>)">
                  <?php
                   $task_followed=DB::select('select * from task_follows'); 
                   foreach ($task_followed as $task_follow) {
                     if ($user_id==$task_follow->follower_id && $task_key==$task_follow->task_id) {
                      ?>
                      <script type="text/javascript"> document.getElementById("follow_task<?php echo $task_key; ?>").checked = true;</script>
                    <?php }
                   }?>
                   
                   
                   </td>
                  <td><button style="color: yellow;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#attach_file" onclick="show_attachment(<?php echo $task_key; ?>)">
    view/add
</button></td>
      </tr><?php continue; }elseif($access_type==1 || $created_by_id==$user_id || $assign_to_id==$user_id)
      {?>
        <tr>
                  <td><?php echo $task_key ?></td>
                  <td>
          <?php echo $task_name ?>
                  </td>
                  <td><?php echo $task_category ?></td>
                  <td><?php echo $task_priority ?></td>
                  <td><?php echo $due_date ?></td>
          <td style="color: blue;" onclick="show_modal_status(<?php echo $task_key ?>)"><a href="#"><?php echo $status ?></a></td>
                 <td style="color: blue;" onclick="show_modal_done(<?php echo $task_key ?>)"><a href="#"><?php echo $perc_done ?></a></td>
                  <td><?php echo $created_by ?></td>
                  <td style="color: blue;" onclick="show_modal1(<?php echo $assign_to_id ?>,'<?php echo $assign_to ?>')"><a href="#"><?php echo $assign_to ?></a></td>
                  <td><button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalNorm" onclick="assign_task_id(<?php echo $task_key; ?>)">
    view/add
</button></td>
                  <td><input type="checkbox" id="follow_task<?php echo $task_key; ?>"  onchange="follow_task(<?php echo $task_key; ?>)">
                  <?php
                   $task_followed=DB::select('select * from task_follows'); 
                   foreach ($task_followed as $task_follow) {
                     if ($user_id==$task_follow->follower_id && $task_key==$task_follow->task_id) {
                      ?>
                      <script type="text/javascript"> document.getElementById("follow_task<?php echo $task_key; ?>").checked = true;</script>
                    <?php }
                   }?>
                   
                   
                   </td>
                  <td><button style="color: yellow;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#attach_file" onclick="show_attachment(<?php echo $task_key; ?>)">
    view/add
</button></td>
      </tr><?php }} ?>
      
                
                
                </tbody>
                
              </table>