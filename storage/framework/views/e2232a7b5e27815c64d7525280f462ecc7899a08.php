<?php $__env->startSection('content'); ?>
<!-- Main content -->
<section class="content">
      <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <span><h3 style=color:blue;>Select Department</h3></span>
          <select class="selectpicker" onchange="filter_by_department()" id="filter_by_department" data-live-search="true">
          <?php
use Illuminate\Support\Facades\DB;
$users = DB::select('select * from departments');
foreach ($users as $user) {
    $id = $user->id;
    $name=$user->name;
?>
<option data-tokens="ketchup mustard" id="filter_by_department" value="<?php echo $id ?>"><?php echo $name ?></option><?php }?>
  
             </select>
        </div>

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
         <span><h3 style=color:blue;>Select Task</h3></span>
          <select class="selectpicker" data-live-search="true" onchange="filter_by_status()" id="filter_by_status">
  <option data-tokens="ketchup mustard" value="all">all tasks</option>
  <option data-tokens="mustard" value="open">Open tasks</option>
  <option data-tokens="frosting" value="close">Closed tasks</option>
             </select>
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <span><h3 style=color:blue;>Select Acess</h3></span>
          <select id="filter_by_access" onchange="filter_by_access()" class="selectpicker" data-live-search="true">
  <option data-tokens="ketchup mustard" value="0">ALL</option>
  <?php 
  $access_types = DB::select('select * from access_type');
  foreach ($access_types as $access_type) {
    $id = $access_type->id;
    $name=$access_type->name;
  ?>
  <option data-tokens="ketchup mustard" value="<?php echo $id ?>"><?php echo $name ?></option><?php }?>
             </select>
        </div>

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          
 <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>

        </div>
        <!-- /.col -->

    </div>


    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
         
        </div>
        <!-- /.col -->
        
        <!-- fix for small devices only -->

        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          
        </div>
        <!-- /.col -->

        <div class="col-md-3 col-sm-6 col-xs-12">
            <!-- Button trigger modal -->
        
        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalHorizontal" style="margin-bottom:10px;margin-left:50px;">
    Create New Task
</button>

<!-- Modal -->
<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Create Task
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

            <form id="form1" class="form-horizontal" role="form">
                <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="inputEmail3">Task Name</label>
                    <div class="col-sm-10">
                        <input  class="form-control" 
                        id="input_name" placeholder="Enter Task Name"/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="inputEmail3">task description</label>
                    <div class="col-sm-10">
                        <input  class="form-control" 
                        id="input_description" placeholder="Enter Task Description"/>
                    </div>
                  </div>


                  <div class="form-group">
                      <div class="col-sm-10" class="form-control" class="multiselect">
    <span><h3>Select Category</h3></span>
          <select id="category" class="selectpicker" data-live-search="true">
          <?php 
  $categories = DB::select('select * from categories');
  foreach ($categories as $category) {
    $id = $category->id;
    $name=$category->name;
  ?>
  <option  data-tokens="ketchup mustard" value="<?php echo $id ?>"><?php echo $name ?></option><?php } ?>
 
             </select>
  </div>
                  </div>

                  <div class="form-group">
                     <div class="col-sm-10" class="form-control" class="multiselect">
    <span><h3>Select Priority Level</h3></span>
          <select id="priority_level" class="selectpicker" data-live-search="true">
      <?php 
  $priorities = DB::select('select * from priority');
  foreach ($priorities as $priority) {
    $id = $priority->id;
    $name=$priority->priority_name;
  ?>
  <option  data-tokens="ketchup mustard" value="<?php echo $id ?>"><?php echo $name ?></option><?php } ?>
             </select>
  </div>
                  </div>

                  <div class="form-group">
                     <div class="col-sm-10" class="form-control" class="multiselect">
    <span><h3 >Assign To</h3></span>
          <select id="assign_to" class="selectpicker" data-live-search="true">
  <?php 
  $users = DB::select('select * from users limit 5');
  foreach ($users as $user) {
    $id = $user->id;
    $name=$user->name;
    if($id != Auth::user()->id){
  ?>
    <option  data-tokens="ketchup mustard" value="<?php echo $id ?>"><?php echo $name ?></option><?php }} ?>
             </select>
  </div>
                  </div>

                  <div class="form-group">
                     <div class="col-sm-10" class="form-control" class="multiselect">
    <span><h3 >Select Access Type</h3></span>
          <select id="access_type" class="selectpicker" data-live-search="true">
  <?php 
  foreach ($access_types as $access_type) {
    $id = $access_type->id;
    $name=$access_type->name;
  ?>
  <option data-tokens="ketchup mustard" value="<?php echo $id ?>"><?php echo $name ?></option><?php } ?>
             </select>
  </div>
                  </div>

                  <div class="form-group">
                  <label  class="col-sm-2 control-label"
                              for="inputEmail3"><h3 style="color:blue;">Tag Users</h3>(check box to tag)</label>
                    <div class="col-sm-10" class="form-control" class="multiselect">
    <div class="selectBox" onclick="showCheckboxes()">
      <select id="tag_group">
        <option><h3>Tag User Group</h3></option>
      </select>
      <div class="overSelect"></div>
    </div>
    <div id="checkboxes">
    <?php 
  $users = DB::select('select * from users limit 5');
  foreach ($users as $user) {
    $id = $user->id;
    $name=$user->name;
    if($id != Auth::user()->id) {
  ?>
      <label for="<?php echo $id ?>">
  <input type="checkbox" id="<?php echo $id ?>" value="<?php echo $id ?>" name="tag_user"/><?php echo $name ?></label><?php }} ?>
      
    </div>
  </div>
                  </div>

<div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="inputEmail3"><h3 style="color:blue;">Due Date (dd/mm/yy)</h3></label>
                    <div class="col-sm-10">
                        <input id="due_date"  class="form-control" 
                        id="inputEmail3" type="date"/>
                    </div>
                  </div>

            </form>
                
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button id="create" type="button" class="btn btn-primary" onclick="saveTask()" data-dismiss="modal">
                    Create Task
                </button>
            </div>
         </div>
     </div>    
     </div>

        <!-- /.search form -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
      <div class="col-xs-12">
      <div class="box">
          <div class="box-header">
              <h3 class="box-title">Tasks Details <span style="color:blue;font-size: 25px;" >(Please click on blue text in table for more task operations)</span></h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Task key</th>
                  <th>Task Name</th>
                  <th>Task Category</th>
                  <th>Priority</th>
                  <th>Due date</th>
                  <th>Status</th>
                  <th>% Done</th>
                  <th>Created by</th>
                  <th>Assigned to</th>
                  <th>Comments</th>
                  <th>Follow</th>
                  <th>Attachments</th>
                </tr>
                </thead>
                <tbody id="table_data">
                    
                    <?php 
          $tasks = DB::select('select * from tasks');
          foreach ($tasks as $task) {
          $access_type=$task->access_id;
          $task_key = $task->id;
          $task_name=$task->task_name;
          $task_category=$task->category_id;
          $category=DB::select('select * from categories where id = ? limit 1', [$task_category]);
          $task_category=$category[0]->name;
          
          $task_priority=$task->priority_id;
          $assign_to_id=$task->assigned_to_id;
          $priority=DB::select('select * from priority where id = ? limit 1', [$task_priority]);
          $task_priority=$priority[0]->priority_name;
          
          $due_date=$task->due_date;
          $status=$task->status;
          $perc_done=$task->percentage_done;
          
          $created_by_id=$task->created_by_id;
          $user=DB::select('select * from users where id = ? limit 1', [$created_by_id]);
          $created_by=$user[0]->name;

          $assign_to_id=$task->assigned_to_id;
          $user=DB::select('select * from users where id = ? limit 1', [$assign_to_id]);
          $assign_to=$user[0]->name;

          $user_id=Auth::user()->id;

          $user=DB::select('select * from users where id = ? limit 1', [$user_id]);
          $level_id=$user[0]->level_id;
          if($level_id==1)
          {
          ?>
                <tr>
                  <td><?php echo $task_key ?></td>
                  <td>
                  <?php echo $task_name ?>
                  </td>
                  <td><?php echo $task_category ?></td>
                  <td><?php echo $task_priority ?></td>
                  <td><?php echo $due_date ?></td>
                  <td style="color: blue;" onclick="show_modal_status(<?php echo $task_key ?>)"><a href="#"><?php echo $status ?></a></td>
                 <td style="color: blue;" onclick="show_modal_done(<?php echo $task_key ?>)"><a href="#"><?php echo $perc_done ?></a></td>
                  <td><?php echo $created_by ?></td>
                  <td style="color: blue;" onclick="show_modal1(<?php echo $assign_to_id ?>,'<?php echo $assign_to ?>')"><a href="#"><?php echo $assign_to ?></a></td>
                  <td><button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalNorm" onclick="assign_task_id(<?php echo $task_key; ?>)">
    view/add
</button></td>
                  <td><input type="checkbox" id="follow_task<?php echo $task_key; ?>"  onchange="follow_task(<?php echo $task_key; ?>)">
                  <?php
                   $task_followed=DB::select('select * from task_follows'); 
                   foreach ($task_followed as $task_follow) {
                     if ($user_id==$task_follow->follower_id && $task_key==$task_follow->task_id) {
                      ?>
                      <script type="text/javascript"> document.getElementById("follow_task<?php echo $task_key; ?>").checked = true;</script>
                    <?php }
                   }?>
                   
                   
                   </td>
                  <td><button style="color: yellow;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#attach_file" onclick="show_attachment(<?php echo $task_key; ?>)">
    view/add
</button></td>
          </tr><?php continue; }elseif($access_type==1 || $created_by_id==$user_id || $assign_to_id==$user_id)
          {?>
              <tr>
                  <td><?php echo $task_key ?></td>
                  <td>
                  <?php echo $task_name ?>
                  </td>
                  <td><?php echo $task_category ?></td>
                  <td><?php echo $task_priority ?></td>
                  <td><?php echo $due_date ?></td>
                  <td style="color: blue;" onclick="show_modal_status(<?php echo $task_key ?>)"><a href="#"><?php echo $status ?></a></td>
                 <td style="color: blue;" onclick="show_modal_done(<?php echo $task_key ?>)"><a href="#"><?php echo $perc_done ?></a></td>
                  <td><?php echo $created_by ?></td>
                  <td style="color: blue;" onclick="show_modal1(<?php echo $assign_to_id ?>,'<?php echo $assign_to ?>')"><a href="#"><?php echo $assign_to ?></a></td>
                  <td><button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalNorm" onclick="assign_task_id(<?php echo $task_key; ?>)">
    view/add
</button></td>
                  <td><input type="checkbox" id="follow_task<?php echo $task_key; ?>"  onchange="follow_task(<?php echo $task_key; ?>)">
                  <?php
                   $task_followed=DB::select('select * from task_follows'); 
                   foreach ($task_followed as $task_follow) {
                     if ($user_id==$task_follow->follower_id && $task_key==$task_follow->task_id) {
                      ?>
                      <script type="text/javascript"> document.getElementById("follow_task<?php echo $task_key; ?>").checked = true;</script>
                    <?php }
                   }?>
                   
                   
                   </td>
                  <td><button style="color: yellow;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#attach_file" onclick="show_attachment(<?php echo $task_key; ?>)">
    view/add
</button></td>
          </tr><?php }} ?>
          
                
                

                </tbody>
            </table>
                
            </div>
            <!-- /.box-body -->
      </div>
          <!-- /.box -->
           <!-- Button trigger modal -->
      <!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Add/View Comment
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form role="form">
                  <div class="form-group">
                    <p id="previous_comments">Previeous comments appear here..</p>
                  </div>
                  <div class="form-group">
                    <textarea rows="4" cols="50" id="comment">
                    </textarea>
                  </div>
                </form>
                
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" onclick="save_comment()">
                    Save Comment
                </button>
            </div>
        </div>
    </div>
</div>

     <!-- Modal -->
<div class="modal fade" id="attach_file" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    View/Add Attachments
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" id="attachments">
                
                <form role="form">
                  <div class="form-group">
                    <p id="previous_comments">Attachments appear here..</p>
                  </div>
                  
                </form>
                
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>


          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row (main row) -->

<!-- Modal 1 -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1Label" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-left">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                    </button>
                     <h4 class="modal-title" id="modal1Label">Left Modal title</h4>

                </div>
                <div class="modal-body" id="modal_append"><span>Check to Follow</span>   <input type="checkbox" id="emp_follow" onchange="follow_employee()"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>