<?php $__env->startSection('content'); ?>
<!-- Main content -->
<!-- Table content -->
<h1 style="color: blue;">REPORT OF OPEN TASKS</h1>
<table  class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Task key</th>
                  <th>Task Name</th>
                  <th>Task Category</th>
                  <th>Priority</th>
                  <th>Due date</th>
				  <th>Status</th>
				  <th>% Done</th>
				  <th>Created by</th>
				  <th>Assigned to</th>
				  
				  
                </tr>
                </thead>
                <tbody id="table_data">
				
                <?php 
          $tasks = DB::select('select * from tasks where status = ?',["open"]);
          foreach ($tasks as $task) {
		  $access_type=$task->access_id;
          $task_key = $task->id;
	      $task_name=$task->task_name;
		  $task_category=$task->category_id;
		  $category=DB::select('select * from categories where id = ? limit 1', [$task_category]);
		  $task_category=$category[0]->name;
		  
		  $task_priority=$task->priority_id;
		  $priority=DB::select('select * from priority where id = ? limit 1', [$task_priority]);
		  $task_priority=$priority[0]->priority_name;
		  
		  $due_date=$task->due_date;
		  $status=$task->status;
		  $perc_done=$task->percentage_done;
		  
		  $created_by_id=$task->created_by_id;
		  $user=DB::select('select * from users where id = ? limit 1', [$created_by_id]);
		  $created_by=$user[0]->name;
		  
		  $assign_to_id=$task->assigned_to_id;
		  $user=DB::select('select * from users where id = ? limit 1', [$assign_to_id]);
		  $assign_to=$user[0]->name;
		  
		  
          ?>
                <tr>
                  <td><?php echo $task_key ?></td>
                  <td>
				  <?php echo $task_name ?>
                  </td>
                  <td><?php echo $task_category ?></td>
                  <td><?php echo $task_priority ?></td>
                  <td><?php echo $due_date ?></td>
				  <td><?php echo $status ?></td>
                 <td  ><?php echo $perc_done ?></td>
                  <td><?php echo $created_by ?></td>
                  <td  ><?php echo $assign_to ?></td>
                  
                  
                  
		  </tr><?php   } ?>

                </tbody>
              <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="/reports" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
              </table>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>