<!DOCTYPE html>
<head>
<script type="text/javascript">

// show file attachments

function show_attachment(task_id){
  console.log(task_id);

  $.ajax({
    method: 'GET',
    url: 'fileentry',
    dataType: 'html',
    data: {"task_id":task_id}, 
  success: function(data) {
        if(data.error) {
            alert("Oops...", data.data, "error");
        } else {
          $('#attachments').html(data);
            console.log(data);
            $('#attach_task_id').val(task_id);
        }
    },
    error: function(html, status) {
        console.log(html.responseText);
        console.log(status);
    } 
  });
}

// assign task id to modal in order to use when commenting
function assign_task_id(task_id)
{
$('div#myModalNorm').val(task_id);
var user_id=<?php echo e(Auth::user()->id); ?>;
$.ajax({
    method: 'GET',
    url: 'get_comments',
    dataType: 'html',
    data: {"task_id":task_id,"user_id":user_id}, 
  success: function(data) {
        if(data.error) {
            alert("Oops...", data.data, "error");
        } else {
          $('#previous_comments').html(data);
            console.log(data);
            
        }
    },
    error: function(html, status) {
        console.log(html.responseText);
        console.log(status);
    } 
  });

}

// send comment to the db using ajax
function save_comment()
{
  var text = $('textarea#comment').val();
  var user_id=<?php echo e(Auth::user()->id); ?>;
  var task_id=$('div#myModalNorm').val();
   

  var token;
token='<?php echo e(csrf_token()); ?>';

  var data = {"task_id":task_id,"user_id":user_id,"comment":text}; 
  $.ajax({
     headers: {
      'X-CSRF-TOKEN': token
},
      type: 'POST',
      url: "task/save_comment",
      data: data,
      dataType: "html",
       success: function(data) {
        if(data.error) {
            alert("Oops...", data.data, "error");
        } else {
            console.log(data);
            
        }
    },
    error: function(html, status) {
        console.log(html.responseText);
        console.log(status);
    }
});

}

function follow_task(task_id)
{
  
  var user_id=<?php echo e(Auth::user()->id); ?>;
   
   var token;
token='<?php echo e(csrf_token()); ?>';

  var x = document.getElementById('follow_task'+task_id).checked;
  var check_status=0;
  console.log(x);
  if (x) {
    check_status=1;
  }
  var data = {"task_id":task_id,"user_id":user_id,"check_status":check_status}; 
  $.ajax({
     headers: {
      'X-CSRF-TOKEN': token
},
      type: 'POST',
      url: "task/follow",
      data: data,
      dataType: "html",
       success: function(data) {
        if(data.error) {
            sweetAlert("Oops...", data.data, "error");
        } else {
            console.log(data);
            
        }
    },
    error: function(html, status) {
        console.log(html.responseText);
        console.log(status);
    }
});

}


//show modal to follow employee and fetch folow status

function show_modal1(id,name)
{

  var follower_id=<?php echo e(Auth::user()->id); ?>;
  var employee_id=id;
  


  $('input#emp_follow').val(id);
  $("h4#modal1Label").text("Follow/Unfolow "+name);
    $('#modal1').modal({
        show: true,
        backdrop: false
    });
 
 $.ajax({
    method: 'GET',
    url: 'fetch_follow_status',
    dataType: 'html',
    data: {"follower_id":follower_id,"employee_id":employee_id}, 
  success: function(data) {
        if(data.error) {
            alert("Oops...", data.data, "error");
        } else {
            if (data == 1) {
              document.getElementById("emp_follow").checked = true;
            }else{
              document.getElementById("emp_follow").checked = false;
            }
        }
    },
    error: function(html, status) {
        console.log(html.responseText);
        console.log(status);
    } 
  });


}


//show modal showing percentage done

function show_modal_done(task_id)
{

  
  var id = task_id;


  $("h4#modal1Label").text("Please select Percentage done");
    $('#modal1').modal({
        show: true,
        backdrop: false
    });
 
 $( "#modal_append" ).html('<span>Select Percentage done</span> <select id="task_done" onchange="submit_task_done('+task_id+')"><option value="0%">0%</option><option value="5%">5%</option><option value="20%">20%</option><option value="40%">40%</option><option value="60%">60%</option><option value="100%">100%</option></select>');


}

//function to update perce done

function submit_task_done(task_id)
{
  var perc_done=$( "#task_done" ).val();
  $.ajax({
    method: 'GET',
    url: 'update_task_done',
    dataType: 'html',
    data: {"task_id":task_id,"perc_done":perc_done}, 
success: function(data){
  $('#example2').html(data);
      location.reload();
    } 
  });
}

// Show status of task whether open closed or overdue

function show_modal_status(task_id)
{

  
  var id = task_id;


  $("h4#modal1Label").text("Please select Task status");
    $('#modal1').modal({
        show: true,
        backdrop: false
    });
 
 $( "#modal_append" ).html('<span>Open/Close Task</span> <select id="task_status" onchange="submit_task_status('+task_id+')"><option value="open">Open</option><option value="close">Close</option></select>');


}

// function to submit task status,whether open or closed
function submit_task_status(task_id)
{

  var task_status=$( "#task_status" ).val();
  $.ajax({
    method: 'GET',
    url: 'update_task_status',
    dataType: 'html',
    data: {"task_id":task_id,"task_status":task_status}, 
success: function(data){
  $('#example2').html(data);
      location.reload();
    } 
  });

}

// follow/unfollow employee
function follow_employee() {
  var follower_id=<?php echo e(Auth::user()->id); ?>;
  var employee_id=$('input#emp_follow').val();
  
  var check_status = document.getElementById('emp_follow').checked;


  var token;
token='<?php echo e(csrf_token()); ?>';

  var data = {"follower_id":follower_id,"employee_id":employee_id,"check_status":check_status}; 
  $.ajax({
     headers: {
      'X-CSRF-TOKEN': token
},
      type: 'POST',
      url: "employee/follow",
      data: data,
      dataType: "html",
       success: function(data) {
        if(data.error) {
            sweetAlert("Oops...", data.data, "error");
        } else {
            console.log(data);
            
        }
    },
    error: function(html, status) {
        console.log(html.responseText);
        console.log(status);
    }
});


}

// create task
function saveTask()
{
  var taged_users = [];
            $.each($("input[name='tag_user']:checked"), function(){            
                taged_users.push($(this).val());
            });
    //alert("Taged users are: " + taged_users.join(", "));
  var user_id=<?php echo e(Auth::user()->id); ?>;
  var department_id=<?php echo e(Auth::user()->department_id); ?>;
  //console.log("hey jimmee,how is your night");
  var task_name=$('#input_name').val();
  var task_description=$('#input_description').val();
  var category=$('#category').val();
  var priority_level=$('#priority_level').val();
  var assign_to=$('#assign_to').val();
  var access_type=$('#access_type').val();
  var dateControl = document.querySelector('input[type="date"]');
  var due_date=dateControl.value;
  console.log(category);
  console.log(due_date);
  
  var token;
token='<?php echo e(csrf_token()); ?>';
console.log(token);

  var user_group = [];
  $('#checkboxes :input').each(function(i)
  { if(this.checked)
      {
    user_group[i] = $(this).val();
         }   
        });
    console.log(user_group);
  var data = {"user_id": user_id, "department_id"

:department_id,"task_name": task_name, "task_description": task_description, "category": category,"priority_level":priority_level,"assign_to":assign_to,"access_type":access_type,"due_date":due_date,"user_group":user_group}; 
  console.log(data);
  $.ajax({
     headers: {
      'X-CSRF-TOKEN': token
},
      type: 'POST',
      url: "task",
      data: data,
      dataType: "html",
       success: function(data) {
        alert("Task Created successfully.");
        location.reload();
    }
});
}



var expanded = false;
	function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

function filter_by_department()
{
	var department_id=$('#filter_by_department').val();
	var user_id= <?php echo e(Auth::user()->id); ?> ;
	$.ajax({
    method: 'GET',
    url: 'task_department',
    dataType: 'html',
    data: {"department_id":department_id,"user_id":user_id}, 
success: function(data){
	$('#example2').html(data);
      console.log('succes: '+data);
    }	
  });
}

function filter_by_status(){
  var status=$('#filter_by_status').val();
  var user_id= <?php echo e(Auth::user()->id); ?> ;
  $.ajax({
    method: 'GET',
    url: 'task_status',
    dataType: 'html',
    data: {"status":status,"user_id":user_id}, 
success: function(data){
  $('#example2').html(data);
      console.log('succes: '+data);
    } 
  });
}

function filter_by_access()
{
	var access_id=$('#filter_by_access').val();
	var user_id=<?php echo e(Auth::user()->id); ?>;
	$.ajax({
    url: 'task_access',
    method: 'GET',
    dataType: 'html',
    data: {"access_id":access_id,"user_id":user_id}, 
success: function(data){
	$('#example2').html(data);
      console.log('succes: '+data);
    }	
  });
}
</script>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TaskPortal | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/font-awesome/css/font-awesome.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/Ionicons/css/ionicons.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('dist/css/AdminLTE.min.css')); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo e(asset('dist/css/skins/_all-skins.min.css')); ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/morris.js/morris.css')); ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/jvectormap/jquery-jvectormap.css')); ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo e(asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  <style>
  .modal-body .form-horizontal .col-sm-2,
.modal-body .form-horizontal .col-sm-10 {
    width: 100%
}

.modal-body .form-horizontal .control-label {
    text-align: left;
}
.modal-body .form-horizontal .col-sm-offset-2 {
    margin-left: 15px;
}
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}

.multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div id="wrapper" class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>T</b>P</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>TASK</b>PORTAL</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo e(url('dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo e(asset('dist/img/user3-128x128.jpg')); ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo e(asset('dist/img/user4-128x128.jpg')); ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo e(asset('dist/img/user3-128x128.jpg')); ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo e(asset('dist/img/user4-128x128.jpg')); ?>" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <?php  $employee_id=Auth::user()->id; $notifications=DB::select('select * from notifications where employee_id = ? and seen_status=?', [$employee_id,0]); ?>
              <span class="label label-warning"><?php echo $size=sizeof($notifications) ?></span> 
            </a>
            <ul class="dropdown-menu">
           
              <li class="header">You have <?php echo $size ?> notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                 
                 <?php if($size>0) { foreach($notifications as $notify) { ?>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> <?php echo $notify->notification ?>
                    </a>
                  </li>
                 <?php }} ?>
                </ul>
              </li>
              
            </ul>
          </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo e(asset('dist/img/user2-160x160.jpg')); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"> <?php echo e(Auth::user()->name); ?> </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo e(asset('dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">

              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                
                <div class="pull-right">
                  <a href="<?php echo e(route('login')); ?>" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>

                                                     <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo e(asset('dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo e(Auth::user()->name); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="/home/dashboard"><i class="fa fa-book"></i> <span>Dashboard</span></a></li>
           
        <li><a href="/home/dashboard"><i class="fa fa-folder"></i> <span>Projects</span></a></li>
        <li><a href="/home/dashboard"><i class="fa fa-folder"></i> <span>Myprojects</span></a></li>
        
        <li><a href="/home/dashboard"><i class="fa fa-folder"></i> <span>Issues</span></a></li>
        
        <li><a href="/home/dashboard"><i class="fa fa-folder"></i> <span>UserBoards</span></a></li>
        
        <li class="treeview">
          <a href="/tasks">
            <i class="fa fa-folder"></i> <span>Task & FollowUp</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/tasks"><i class="fa fa-circle-o"></i> Tasks</a></li>
            <li><a href="/tasks"><i class="fa fa-circle-o"></i> UserBoards</a></li>
          </ul>
        </li>

        <?php
        if(Auth::user()->level_id==1) { ?>
        <li><a href="/reports"><i class="fa fa-book"></i> <span>Reports</span></a></li>
        <?php } ?>
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <?php echo $__env->yieldContent('content'); ?>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo e(asset('bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo e(asset('bower_components/jquery-ui/jquery-ui.min.js')); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(asset('bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo e(asset('bower_components/raphael/raphael.min.js')); ?>"></script>
<script src="<?php echo e(asset('bower_components/morris.js/morris.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(asset('bower_components/jquery-knob/dist/jquery.knob.min.js')); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo e(asset('bower_components/moment/min/moment.min.js')); ?>"></script>
<script src="<?php echo e(asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('bower_components/fastclick/lib/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('dist/js/adminlte.min.js')); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo e(asset('dist/js/pages/dashboard.js')); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('dist/js/demo.js')); ?>"></script>
<script type="text/javascript">

  
</script>
</body>
</html>
