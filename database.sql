-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 02, 2018 at 08:48 AM
-- Server version: 5.5.56-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_type`
--

CREATE TABLE IF NOT EXISTS `access_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_type`
--

INSERT INTO `access_type` (`id`, `name`, `description`) VALUES
(1, 'public', 'ajkasks'),
(2, 'private', 'asam');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, ' Web Design', 'assjaskjs', '2018-07-28 15:42:11', '0000-00-00'),
(2, 'Documentation', 'ssasmasma,', '2018-07-28 15:42:11', '0000-00-00'),
(3, 'Sales', 'zxklpzx[xkz', '2018-07-28 15:42:11', '0000-00-00'),
(4, 'Engineering', 'lkzkxnzm,nde dc', '2018-07-28 15:42:11', '0000-00-00'),
(5, 'Customer Care', 'xmjasccnjskdj', '2018-07-28 15:42:11', '0000-00-00'),
(6, 'Default', 'dsdldsdkmezn', '2018-07-28 15:42:11', '0000-00-00'),
(7, 'index.php', 'index.php', '2018-07-31 12:30:19', '2018-07-31'),
(8, 'aasdssd', 'aasdssd', '2018-07-31 12:30:46', '2018-07-31');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `commenter_id` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `task_id`, `commenter_id`, `comment`, `date_created`) VALUES
(1, 1, 1, 'Finish by Friday', '2018-05-16'),
(8, 1, 1, 'add name to task details', '2018-05-21'),
(9, 50, 1, 'zx', '2018-05-22');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `details` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `details`) VALUES
(1, 'ICT', 'nsdaldka'),
(2, 'Human Resource', 'asasalsk'),
(3, 'Finance', 'saksjskasj'),
(4, 'Marketing', 'askjaska'),
(5, 'Production', 'laska');

-- --------------------------------------------------------

--
-- Table structure for table `employee_follows`
--

CREATE TABLE IF NOT EXISTS `employee_follows` (
  `id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `follow_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_follows`
--

INSERT INTO `employee_follows` (`id`, `follower_id`, `employee_id`, `date_created`, `follow_status`) VALUES
(39, 1, 2, '2018-05-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fileentries`
--

CREATE TABLE IF NOT EXISTS `fileentries` (
  `id` int(10) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `task_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fileentries`
--

INSERT INTO `fileentries` (`id`, `filename`, `mime`, `original_filename`, `created_at`, `updated_at`, `task_id`) VALUES
(1, 'phpB2B9.tmp.PNG', 'image/png', 'Capture.PNG', '2018-04-25 07:07:18', '2018-04-25 07:07:18', 1),
(2, 'phpD90E.tmp.DOCX', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'USSD STRUCTURE 2018 FOR CUSTOMER CARE.DOCX', '2018-04-25 07:07:28', '2018-04-25 07:07:28', 1),
(3, 'phpAE59.tmp.zip', 'application/x-zip-compressed', 'Softinu-Zip-File-Password.zip', '2018-04-25 09:19:27', '2018-04-25 09:19:27', 2),
(4, 'php394.tmp.jpg', 'image/jpeg', 'cert_photoshop.jpg', '2018-05-20 07:20:11', '2018-05-20 07:20:11', 4),
(5, 'phpD42.tmp.sql', 'application/octet-stream', 'got.sql', '2018-05-20 07:28:57', '2018-05-20 07:28:57', 1),
(6, 'php70AF.tmp.PNG', 'image/png', 'Capture3.PNG', '2018-05-20 10:25:14', '2018-05-20 10:25:14', 4),
(7, 'php6F2D.tmp.txt', 'text/plain', 'xode.txt', '2018-05-20 10:45:59', '2018-05-20 10:45:59', 1),
(8, 'phpCFCB.tmp.DOCX', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'USSD STRUCTURE 2018 FOR CUSTOMER CARE.DOCX', '2018-05-20 11:33:22', '2018-05-20 11:33:22', 3),
(9, 'phpBF18.tmp.tif', 'image/tiff', 'WelcomeFax.tif', '2018-05-20 13:09:55', '2018-05-20 13:09:55', 2),
(10, 'php59D2.tmp.PNG', 'image/png', 'quiz2.PNG', '2018-05-20 13:21:30', '2018-05-20 13:21:30', 2),
(11, 'phpB750.tmp.PNG', 'image/png', 'quiz1.PNG', '2018-05-20 13:22:59', '2018-05-20 13:22:59', 1),
(12, 'php5898.tmp.jpg', 'image/jpeg', 'Untitled-1.jpg', '2018-05-20 14:18:17', '2018-05-20 14:18:17', 1),
(13, 'phpFD3.tmp.PNG', 'image/png', 'space.PNG', '2018-05-20 14:19:04', '2018-05-20 14:19:04', 3),
(14, 'phpC2B9.tmp.PNG', 'image/png', 'quiz3.PNG', '2018-05-20 14:19:50', '2018-05-20 14:19:50', 2),
(15, 'phpPB9mL4.xml', 'text/xml', 'AndroidManifest.xml', '2018-05-21 15:16:26', '2018-05-21 15:16:26', 1),
(16, 'phpZ0r3GN.xml', 'text/xml', 'AndroidManifest.xml', '2018-05-22 10:27:12', '2018-05-22 10:27:12', 51);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `notification` longtext NOT NULL,
  `seen_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `task_name` varchar(50) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `employee_id`, `notification`, `seen_status`, `created_at`, `task_name`, `updated_at`) VALUES
(57, 2, 'Tagged on new task', 1, '2018-08-01 12:35:53', 'jksdjk', '2018-08-01 14:22:21'),
(58, 3, 'Tagged on new task', 0, '2018-08-01 12:35:53', 'jksdjk', '0000-00-00 00:00:00'),
(59, 4, 'Tagged on new task', 0, '2018-08-01 12:35:53', 'jksdjk', '0000-00-00 00:00:00'),
(60, 1, 'Tagged on new task', 1, '2018-08-01 12:46:17', 'ksdjk', '2018-07-31 21:00:00'),
(61, 2, 'Tagged on new task', 0, '2018-08-01 12:46:17', 'ksdjk', '0000-00-00 00:00:00'),
(62, 4, 'Tagged on new task', 0, '2018-08-01 12:46:18', 'ksdjk', '0000-00-00 00:00:00'),
(63, 1, 'New Task Created', 1, '2018-08-01 12:46:18', 'ksdjk', '2018-08-01 10:45:00'),
(64, 3, 'Tagged on new task', 0, '2018-08-01 13:37:49', 'jkewkek', '2018-08-01 13:37:49'),
(65, 4, 'Tagged on new task', 0, '2018-08-01 13:37:49', 'jkewkek', '2018-08-01 13:37:49'),
(66, 3, 'New Task Created', 0, '2018-08-01 13:37:49', 'jkewkek', '2018-08-01 13:37:49'),
(67, 2, 'Tagged on new task', 0, '2018-08-01 14:20:45', 'Jamo', NULL),
(68, 3, 'Tagged on new task', 0, '2018-08-01 14:20:45', 'Jamo', NULL),
(69, 4, 'Tagged on new task', 0, '2018-08-01 14:20:45', 'Jamo', NULL),
(70, 3, 'New Task Created', 0, '2018-08-01 14:20:45', 'Jamo', NULL),
(71, 3, 'Tagged on new task', 0, '2018-08-02 07:48:55', 'task3', NULL),
(72, 4, 'Tagged on new task', 0, '2018-08-02 07:48:55', 'task3', NULL),
(73, 2, 'New Task Created', 0, '2018-08-02 07:48:55', 'task3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jameskae69@gmail.com', '$2y$10$fDLfU2H6fAYRGMs9bj7GtuhH547iz0cMZxbAC8V3IPR6TvGzeoHCu', '2018-04-23 10:15:26'),
('jameskae69@gmail.com', '$2y$10$fDLfU2H6fAYRGMs9bj7GtuhH547iz0cMZxbAC8V3IPR6TvGzeoHCu', '2018-04-23 10:15:26'),
('admin@demo.com', '$2y$10$qIB81LBtiGP2a/SGS2303uNsVP8GfN53Aj0YCwa4VLaqfzAcIhE.C', '2018-07-26 18:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE IF NOT EXISTS `priority` (
  `id` int(11) NOT NULL,
  `priority_name` varchar(20) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`id`, `priority_name`, `description`) VALUES
(1, 'Priority Level 1', 'ajksaksjkja'),
(2, 'Priority Level 2', 'asjskaj'),
(3, 'Priority Level 3', 'jasjk'),
(4, 'Priority Level 4', 'sjasjaj'),
(5, 'Priority Level 5', 'ajakjk');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `leader` varchar(50) NOT NULL,
  `creator` varchar(50) NOT NULL,
  `details` varchar(50) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `status`, `leader`, `creator`, `details`, `created_at`, `updated_at`) VALUES
(1, 'Project 1', 'opened', 'Jon Snow', 'Jon Snow', 'anssasnm', '2018-07-10', '2018-07-18'),
(2, 'sdsd34343', 'opened', 'Jaime Lanni', 'Khal Drogo', 'dsfff', '2018-08-02', '2018-08-02');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `description`) VALUES
(1, 'closed', 'adan'),
(2, 'opened', 'asnasns'),
(3, 'Completed', 'asaja');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `tagged_by_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `employee_id`, `task_id`, `tagged_by_id`, `date_created`) VALUES
(8, 3, 56, 1, '2018-07-29 19:22:29'),
(9, 4, 56, 1, '2018-07-29 19:22:29'),
(10, 3, 57, 1, '2018-07-29 20:31:50'),
(11, 4, 57, 1, '2018-07-29 20:31:50'),
(12, 2, 58, 1, '2018-07-30 15:15:50'),
(13, 4, 58, 1, '2018-07-30 15:15:50'),
(14, 2, 59, 1, '2018-07-30 15:21:23'),
(15, 3, 59, 1, '2018-07-30 15:21:23'),
(16, 4, 59, 1, '2018-07-30 15:21:24'),
(17, 2, 60, 1, '2018-07-30 15:27:06'),
(18, 3, 60, 1, '2018-07-30 15:27:06'),
(19, 4, 60, 1, '2018-07-30 15:27:06'),
(20, 2, 61, 1, '2018-07-30 15:28:11'),
(21, 3, 61, 1, '2018-07-30 15:28:11'),
(22, 3, 62, 1, '2018-07-30 21:55:58'),
(23, 3, 63, 1, '2018-07-30 22:59:46'),
(24, 4, 63, 1, '2018-07-30 22:59:46'),
(25, 2, 64, 1, '2018-07-30 23:02:19'),
(26, 3, 64, 1, '2018-07-30 23:02:19'),
(27, 4, 64, 1, '2018-07-30 23:02:20'),
(28, 2, 65, 1, '2018-07-30 23:03:06'),
(29, 3, 65, 1, '2018-07-30 23:03:06'),
(30, 4, 65, 1, '2018-07-30 23:03:06'),
(31, 2, 66, 1, '2018-07-30 23:03:40'),
(32, 3, 66, 1, '2018-07-30 23:03:40'),
(33, 4, 66, 1, '2018-07-30 23:03:40'),
(34, 2, 67, 1, '2018-07-30 23:05:27'),
(35, 3, 67, 1, '2018-07-30 23:05:27'),
(36, 2, 68, 1, '2018-07-30 23:06:29'),
(37, 3, 68, 1, '2018-07-30 23:06:29'),
(38, 2, 69, 1, '2018-07-30 23:07:23'),
(39, 3, 69, 1, '2018-07-30 23:07:23'),
(40, 2, 70, 1, '2018-07-30 23:07:57'),
(41, 3, 70, 1, '2018-07-30 23:07:57'),
(42, 1, 71, 3, '2018-08-01 11:27:24'),
(43, 2, 71, 3, '2018-08-01 11:27:24'),
(44, 4, 71, 3, '2018-08-01 11:27:24'),
(45, 1, 72, 3, '2018-08-01 11:28:08'),
(46, 2, 72, 3, '2018-08-01 11:28:08'),
(47, 4, 72, 3, '2018-08-01 11:28:08'),
(48, 1, 73, 3, '2018-08-01 11:28:40'),
(49, 2, 73, 3, '2018-08-01 11:28:40'),
(50, 4, 73, 3, '2018-08-01 11:28:40'),
(51, 1, 74, 3, '2018-08-01 11:29:34'),
(52, 2, 74, 3, '2018-08-01 11:29:34'),
(53, 4, 74, 3, '2018-08-01 11:29:34'),
(54, 1, 75, 3, '2018-08-01 11:30:17'),
(55, 2, 75, 3, '2018-08-01 11:30:17'),
(56, 4, 75, 3, '2018-08-01 11:30:17'),
(57, 1, 76, 3, '2018-08-01 11:31:24'),
(58, 2, 76, 3, '2018-08-01 11:31:24'),
(59, 4, 76, 3, '2018-08-01 11:31:24'),
(60, 2, 77, 1, '2018-08-01 12:35:53'),
(61, 3, 77, 1, '2018-08-01 12:35:53'),
(62, 4, 77, 1, '2018-08-01 12:35:53'),
(63, 1, 78, 3, '2018-08-01 12:46:17'),
(64, 2, 78, 3, '2018-08-01 12:46:17'),
(65, 4, 78, 3, '2018-08-01 12:46:17'),
(66, 3, 79, 1, '2018-08-01 13:36:25'),
(67, 3, 80, 1, '2018-08-01 13:36:50'),
(68, 3, 81, 1, '2018-08-01 13:37:49'),
(69, 4, 81, 1, '2018-08-01 13:37:49'),
(70, 2, 82, 1, '2018-08-01 14:20:45'),
(71, 3, 82, 1, '2018-08-01 14:20:45'),
(72, 4, 82, 1, '2018-08-01 14:20:45'),
(73, 3, 83, 1, '2018-08-02 07:48:55'),
(74, 4, 83, 1, '2018-08-02 07:48:55');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL,
  `task_name` varchar(20) DEFAULT NULL,
  `priority_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `assigned_to_id` int(11) DEFAULT NULL,
  `status_id` varchar(10) DEFAULT NULL,
  `percentage_done` varchar(10) NOT NULL DEFAULT '0 %',
  `access_type_id` int(11) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `priority_id`, `category_id`, `created_by_id`, `assigned_to_id`, `status_id`, `percentage_done`, `access_type_id`, `due_date`, `start_date`, `department_id`, `created_at`, `updated_at`, `task_description`) VALUES
(55, 'sdnmsdsk', 2, 5, 1, 4, '2', '20 %', 2, '2018-07-10', '2018-07-04', 1, '2018-07-29', '2018-07-29 16:21:39', 'asksjajs'),
(59, 'James kae', 2, 4, 1, 2, '1', '100 %', 2, '2018-07-18', '2018-07-17', 1, '2018-07-30', '2018-07-30 18:54:38', ',asxakaXNA,X'),
(62, 'jknsakcnasjk', 4, 3, 1, 3, '1', '50 %', 2, '2018-07-17', '2018-07-17', 1, '2018-07-30', '2018-07-30 18:55:58', 'kasjak'),
(63, 'jfsdkdj', 2, 3, 1, 3, '3', '20 %', 2, '2018-07-16', '2018-07-02', 1, '2018-07-30', '2018-07-30 23:19:04', 'jkasjkaskdj'),
(64, 'hgjhhg', 1, 2, 1, 4, '2', '20 %', 2, '2018-07-09', '2018-07-17', 1, '2018-07-30', '2018-07-30 20:02:19', 'gukkjjh'),
(65, 'hgjhghhg', 4, 4, 1, 3, '2', '20 %', 2, '2018-07-10', '2018-07-11', 1, '2018-07-30', '2018-07-30 20:03:06', 'fhjhfhjgjh'),
(66, 'ghjhjhghg', 2, 1, 1, 2, '2', '20 %', 1, '2018-07-09', '2018-07-10', 1, '2018-07-30', '2018-07-30 20:03:40', 'ghhjgh'),
(67, 'hjhgjhg', 4, 3, 1, 3, '2', '50 %', 1, '2018-07-03', '2018-07-03', 1, '2018-07-30', '2018-07-30 20:05:27', 'jkjkgjkh'),
(68, 'hkjkjkjhj', 3, 6, 1, 4, '1', '50 %', 2, '2018-07-17', '2018-07-11', 1, '2018-07-30', '2018-07-30 20:06:29', 'vjhgkjhgj'),
(69, 'hjhkj', 3, 3, 1, 3, '1', '50 %', 1, '2018-07-09', '2018-07-04', 1, '2018-07-30', '2018-07-30 20:07:22', 'gkhjhhj'),
(70, 'hhgjhkbbccd', 5, 2, 1, 2, '1', '50 %', 2, '2018-07-16', '2018-07-04', 1, '2018-07-30', '2018-07-30 20:07:57', 'ghhgcf'),
(71, 'sndkskk', 1, 2, 3, 1, '2', '20 %', 2, '2018-08-14', '2018-08-15', 3, '2018-08-01', '2018-08-01 08:27:24', 'sjhsjh'),
(72, 'sndj', 3, 3, 3, 1, '2', '70 %', 1, '2018-08-28', '2018-08-15', 3, '2018-08-01', '2018-08-01 08:28:08', 'jsdnksjd'),
(73, 'agsgag', 4, 4, 3, 1, '2', '20 %', 2, '2018-08-14', '2018-08-07', 3, '2018-08-01', '2018-08-01 08:28:40', 'sdnmsdnms'),
(74, 'asnsma', 5, 6, 3, 1, '2', '20 %', 2, '2018-08-13', '2018-08-14', 3, '2018-08-01', '2018-08-01 08:29:34', 'ghghjjh'),
(75, 'askska', 4, 4, 3, 1, '2', '20 %', 2, '2018-08-06', '2018-08-14', 3, '2018-08-01', '2018-08-01 08:30:17', 'asksja'),
(76, 'ks;dl', 3, 3, 3, 1, '2', '20 %', 1, '2018-08-07', '2018-08-08', 3, '2018-08-01', '2018-08-01 08:31:24', 'jksjka'),
(77, 'jksdjk', 3, 4, 1, 4, '2', '50 %', 1, '2018-08-15', '2018-08-06', 1, '2018-08-01', '2018-08-01 09:35:53', 'ksjdkdjs'),
(78, 'ksdjk', 3, 4, 3, 1, '1', '20 %', 1, '2018-08-07', '2018-08-07', 3, '2018-08-01', '2018-08-01 09:46:17', 'jmghjhjm'),
(79, 'jkewkek', 3, 4, 1, 3, '2', '20 %', 1, '2018-08-14', '2018-08-22', 1, '2018-08-01', '2018-08-01 10:36:25', 'klekw'),
(80, 'jkewkeki', 3, 4, 1, 3, '2', '20 %', 1, '2018-08-14', '2018-08-22', 1, '2018-08-01', '2018-08-02 07:46:01', 'klekw'),
(81, 'jkewkek', 3, 4, 1, 3, '2', '20 %', 1, '2018-08-14', '2018-08-22', 1, '2018-08-01', '2018-08-01 10:37:49', 'klekw'),
(82, 'Jamo', 2, 3, 1, 3, '2', '20 %', 2, '2018-08-21', '2018-08-07', 1, '2018-08-01', '2018-08-01 14:20:45', 'amdnsdsksjd'),
(83, 'task3', 3, 6, 1, 2, '3', '0 %', 1, '2018-08-07', '2018-08-08', 1, '2018-08-02', '2018-08-02 07:48:55', 'ksdjksdjk');

-- --------------------------------------------------------

--
-- Table structure for table `task_follows`
--

CREATE TABLE IF NOT EXISTS `task_follows` (
  `id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_follows`
--

INSERT INTO `task_follows` (`id`, `follower_id`, `task_id`, `date_created`) VALUES
(2, 1, 2, '2018-05-16'),
(10, 1, 20, '2018-05-19'),
(38, 1, 7, '2018-05-19'),
(39, 1, 6, '2018-05-19'),
(68, 1, 4, '2018-05-19'),
(69, 1, 5, '2018-05-19'),
(70, 1, 3, '2018-05-21'),
(71, 1, 51, '2018-05-22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`, `department_id`, `position`) VALUES
(1, 'Jon Snow', 'admin@demo.com', '$2y$10$i0LpT/9pOOwyeAEVei7rGOcT1mnralaAjCSxU61l5GpFyUN2t0hu6', 'yBVQwtUl1tkPW85t2VYgMXErm21npGlBDL4sFZbJXpnB4G6FBoDnNwZUEGHq', '2018-04-17 05:49:29', '2018-04-17 05:49:29', 'admin', 1, 'manager'),
(2, 'Khal Drogo', 'user@demo.com', '$2y$10$1ldwXN9PZXdDbGH746NZe.YhvregBAt3ZV4i7lbVoQ7K9NwDgmJSi', 'hniYCli7v9eIAAwPNASYUxQJ3h8YLfapYmIVx0DcVOO17Vlrg6ciu6u5gJQ9', '2018-03-26 07:12:19', '2018-03-26 07:12:19', 'user', 1, 'employee'),
(3, 'Jaime Lanni', 'admin1@demo.com', '$2y$10$fUsF9UDD1nsbxVtSkTlz..H6FA8eCccxUvb9qmfm6VQocjqt2a2lq', 'ZHXyIVJ92CCNvlxIJf8p634LQsmJ7U7XIbOi74aD1fGLZlihdQiTUsJTZBmd', '2018-05-18 15:44:44', '2018-05-18 15:44:44', 'admin', 3, 'supervisor'),
(4, 'Catelyn Stark', 'user1@demo.com', '$2y$10$7Cs4HqGR2Aw1Gqqd3TuAfuEPpZfeohxV0yMbFjrjiFYXVtavtMYp.', 'tBGty5zbfggD0TEhOFNR0r4lXsbc7ZzNU8BaW9WWEPMm4ZMfmMfqtq1q4AVS', '2018-05-18 15:46:59', '2018-05-18 15:46:59', 'user', 3, 'employee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_type`
--
ALTER TABLE `access_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_follows`
--
ALTER TABLE `employee_follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fileentries`
--
ALTER TABLE `fileentries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_follows`
--
ALTER TABLE `task_follows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_type`
--
ALTER TABLE `access_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `employee_follows`
--
ALTER TABLE `employee_follows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `fileentries`
--
ALTER TABLE `fileentries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `task_follows`
--
ALTER TABLE `task_follows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
