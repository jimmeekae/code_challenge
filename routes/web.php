<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', 'HomeController@index');

// Authentication routes

Auth::routes();

//default route after login

Route::get('/home', 'HomeController@index');

//load dashbord

Route::get('/dashboard', 'DashboardController@index');




// load tasks 

Route::get('/tasks', 'TasksController@index'); 

Route::post('/item-task', 'TasksController@create'); 

Route::get('/item-task', 'TasksController@tasks');

// get task detail

Route::post('/edittask', 'TasksController@task_detail');

// delete a task

Route::post('/deletetask', 'TasksController@destroy_task');

// filter tasks 

Route::post('/gettask', 'TasksController@filter_task');

// update a task

Route::post('/updatetask', 'TasksController@update_task');

// load my tasks 

Route::get('/mytask', 'MyTaskController@index');


// load my tasks 

Route::get('/mytask', 'MyTaskController@index');










// load task categories

Route::get('/categories', 'CategoryController@index');


// load task categories

Route::get('/category', 'CategoryController@categories');


// create task category

Route::post('/category', 'CategoryController@create');

//edit task category

Route::post('/editcategory', 'CategoryController@edit_category');

//update task category

Route::post('/updatecategory', 'CategoryController@update_category');

//update task category

Route::post('/deletecategoty', 'CategoryController@delete_category');



// load employees

Route::get('/employees', 'EmployeeController@index');

// load employees

Route::get('/client', 'EmployeeController@employee_data');




// View departments 

Route::get('/departments', 'DepartmentController@index');

// View departments 

Route::get('/department', 'DepartmentController@dep_data');


// View Projects

Route::get('/projects', 'ProjectController@index');

// Create Projects

Route::post('/projects', 'ProjectController@create');


// return @Json project data

Route::post('/projectdata', 'ProjectController@data');



// View reports

Route::get('/reports', 'ReportController@index');

// View reports

Route::get('/report', 'ReportController@task_report');

// Clear notifications

Route::post('/clear-notification', 'NotificationController@clear');

