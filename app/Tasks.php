<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model {

	 /**
     * Get the category that owns the task.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the department that owns the task.
     */
    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    /**
     * Get the access_type that owns the task.
     */
    public function access_type()
    {
        return $this->belongsTo('App\Access_type');
    }

    /**
     * Get the access_type that owns the task.
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     * Get the user that owns the task.
     */
    public function assigned_by()
    {
        return $this->belongsTo('App\User','created_by_id');

    }

    /**
     * Get the user that owns the task.
     */
    public function assigned_to()
    {
        return $this->belongsTo('App\User','assigned_to_id');

    }

}
