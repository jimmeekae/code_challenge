<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Access_type extends Model {

	protected $table = 'access_type';

	/**
     * Get the tasks for in Category.
     */
    public function tasks()
    {
        return $this->hasMany('App\Tasks');
    }

}
