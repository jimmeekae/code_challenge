<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	/**
     * Get the tasks for in Category.
     */
    public function tasks()
    {
        return $this->hasMany('App\Tasks');
    }

}
