<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

protected $table = 'status';

	/**
     * Get the tasks for Status.
     */
    public function tasks()
    {
        return $this->hasMany('App\Tasks');
    }

}
