<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;

use Auth;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Report View
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = get_categories();
        $status = get_status();
        $users = get_other_users(Auth::id());

        $access_types=get_access_type();

        return view('reports')->with([
            'categories' => $categories,
            'status' => $status,
            'users' => $users,
            'access_types' => $access_types
        ]);
    }

    /**
     * Fetch task report
     *
     * @return \Illuminate\Http\Response
     */
    public function task_report()
    {
        return Tasks::orderBy('id', 'DESC')->with('category','department','access_type','status','assigned_by','assigned_to')->paginate(26);
    }
}
