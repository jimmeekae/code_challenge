<?php

namespace App\Http\Controllers;

use App\Department;

use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('departments');
    }

    /**
     * Fetch all the departments
     *
     * @return \Illuminate\Http\Response
     */
    public function dep_data()
    {
        return Department::orderBy('id', 'DESC')->paginate(6);
    }
}
