<?php

namespace App\Http\Controllers;
use Auth;
use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function clear(Request $request)
    {
        $id=$request->id;
        $notification=Notification::find($id);
        $notification->seen_status=1;
        $notification->save();
        return "cleared";
    }
}
