<?php

namespace App\Http\Controllers;

use App\Project;
use Auth;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects');
    }

    /**
     * Fetch and return project data.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        return Project::orderBy('id', 'DESC')->paginate(6);
    }

    /**
     * Create new Projetc
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $project=new Project;
        $creator=Auth::user()->name;
        $project->name = $request->name;
        $project->leader = $request->leader_id;
        $project->details = $request->details;
        $project->status = $request->status;
        $project->creator = $creator;
        $project->save();

        return $project;


    }
}
