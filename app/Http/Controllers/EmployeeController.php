<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees');
    }


    /**
     * Show employee data
     *
     * @return \Illuminate\Http\Response
     */
    public function employee_data()
    {
        return User::orderBy('id', 'DESC')->paginate(6);
    }
}
