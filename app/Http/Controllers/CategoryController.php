<?php

namespace App\Http\Controllers;
use App\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('categories');
    }

    /**
     * get task categories.
     *
     * @return Json
     */
    public function categories()
    {
        
        return Category::orderBy('id', 'DESC')->paginate(6);
    }

    /**
     * Create a task category
     *
     * @return Json
     */
    public function create(Request $request)
    {
        
         $this->validate($request, [
    'details' => 'required',
    'name' => 'required'
    
]);


         $category=new Category;
         
         $category->name =$request->name;
         $category->description =$request->name;

         $category->save();
         

        return $category;
    }

    public function edit_category(Request $request){
        $id=$request->id;

        return Category::find($id);

    }

    public function update_category(Request $request){
        $id=$request->id;
        $name=$request->name;
        $details=$request->details;

        $task=Category::find($id);
        $task->name=$name;
        $task->description=$details;
        $task->save();
        return "true";
    }

    public function delete_category(Request $request){
        /* Constraint*/
        //Deleting Category breaks the database
        
       /** $id=$request->id;

        return Category::destroy($id);**/
    }
}
