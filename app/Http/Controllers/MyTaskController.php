<?php

namespace App\Http\Controllers;

use Auth;

use App\Tasks;

use Illuminate\Http\Request;

class MyTaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id=Auth::id();
        $tasks= Tasks::where('assigned_to_id', $user_id)
               ->orderBy('id', 'desc')
               ->with('category','department','access_type','status','assigned_by','assigned_to')
               ->get();
        return view('mytask')->with([
            'tasks' => $tasks  
        ]);
    }
}
