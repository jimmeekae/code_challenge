<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Auth;

use App\Tasks;

use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = get_categories();
        $status = get_status();
        $users = get_other_users(Auth::id());

        $access_types=get_access_type();

        return view('tasks')->with([
            'categories' => $categories,
            'status' => $status,
            'users' => $users,
            'access_types' => $access_types
        ]);
    }

    public function create(Request $request){

        $this->validate($request, [
    'name' => 'required',
    'category' => 'required',
    'description' => 'required',
    'start_time' => 'required',
    'end_time' => 'required',
    'status' => 'required',
    'priority' => 'required',
    'assign' => 'required',
    'access' => 'required',
    'done' => 'required'
]);

        $task=new Tasks;
        $task->task_name=$request->name;
        $task->priority_id=$request->priority;
        $task->category_id=$request->category;
        $task->created_by_id=Auth::id();
        $task->assigned_to_id=$request->assign;
        $task->status_id        =$request->status;
        $task->percentage_done=$request->done;
        $task->access_type_id=$request->access;
        $task->due_date=$request->end_time;
        $task->start_date=$request->start_time;
        $task->department_id=Auth::user()->department_id;
        $task->task_description=$request->description;
        $task->save();

        $user_name=Auth::user()->name;
        $user_id=Auth::user()->id;
        $task_name=$request->name;
        $user_group=$request->taged_users;
        $assigned_to=$request->assign;


        // notification to be saved in the notifications table with read status false by default
        $notification_tag="Tagged on new task";

        $task_id = DB::select('SELECT LAST_INSERT_ID() as t_id');
        $t_id=$task_id[0]->t_id;

         // saved tagged employees in the tags table if any

        if(empty($user_group) == false)
        {
            foreach ($user_group as $user) 
            {
                DB::insert('insert into tags (task_id, employee_id, tagged_by_id) values (?,?,?)', array($t_id,$user,$user_id));

        // save notification in the notifications table
                DB::insert('insert into notifications (employee_id, notification,task_name) values (?,?,?)', array($user,$notification_tag,$task_name));
            }
        }

        // Notify the person assigned task
        $notification_tag="New Task Created";
        DB::insert('insert into notifications (employee_id, notification,task_name) values (?,?,?)', array($assigned_to,$notification_tag,$task_name));


        return $task;


    }

    public function tasks(){

        return Tasks::orderBy('id', 'DESC')->with('category','department','access_type','status','assigned_by','assigned_to')->paginate(6);

    }

    public function task_detail(Request $request){
        $id=$request->id;
        return Tasks::find($id)->load('category','department','access_type','status','assigned_by','assigned_to');
    }


    public function destroy_task(Request $request){
        $id=$request->id;
        return Tasks::destroy($id);

    }

    public function update_task(Request $request){
        $id=$request->id;
        $task=Tasks::find($id);
        $task->task_name=$request->name;
        $task->priority_id=$request->priority;
        $task->category_id=$request->category;
        $task->assigned_to_id=$request->assign;
        $task->status_id=$request->status;
        $task->percentage_done=$request->done;
        $task->access_type_id=$request->access;
        $task->due_date=$request->end_time;
        $task->start_date=$request->start_time;
        $task->task_description=$request->description;
         $task->save();

        return "true";


    }

    public function filter_task(Request $request){
      
      if ($request->name=="task") {
           $department_id=$request->data;
          return Tasks::where('department_id', $department_id)
               ->orderBy('id', 'desc')
               ->with('category','department','access_type','status','assigned_by','assigned_to')
               ->get();
      }elseif ($request->name=="category") {
          $department_id=$request->data;
          return Tasks::where('access_type_id', $department_id)
               ->orderBy('id', 'desc')
               ->with('category','department','access_type','status','assigned_by','assigned_to')
               ->get();
      }elseif ($request->name=="status") {
          $department_id=$request->data;
          return Tasks::where('access_type_id', $department_id)
               ->orderBy('id', 'desc')
               ->with('category','department','access_type','status','assigned_by','assigned_to')
               ->get();
      }

    }
}
