<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

	/**
     * Get the tasks  in Department.
     */
    public function tasks()
    {
        return $this->hasMany('App\Tasks');
    }

}
