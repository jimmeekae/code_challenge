<?php

use App\Category;

use App\User;

use App\Status;

use App\Access_type;

use App\Department;



use Illuminate\Support\Facades\DB;

// get task categories

function get_categories(){

	$categories = Category::all();

	return $categories;
	
}

//get other users except current user

function get_other_users($id){

$users = User::all()->except($id);

return $users;
}

// get all the task status from db

function get_status(){
	$status=Status::all();

	return $status;
}

// get access type

function get_access_type(){
	$access_types=Access_type::all();

	return $access_types;
}

// get notifications

function get_notifications($employee_id)
{
	
	$notifications=DB::select('select * from notifications where employee_id = ? and seen_status=?', [$employee_id,0]);

	return $notifications;
}


function get_departments(){
	return Department::all();
}

?>